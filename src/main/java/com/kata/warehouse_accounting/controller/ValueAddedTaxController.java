package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.ValueAddedTaxDTO;
import com.kata.warehouse_accounting.service.ValueAddedTaxService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

@RestController
@RequestMapping("/api/value_added_tax")
@Api(tags = {"Ставка НДС Rest Controller"})
@Tag(name = "Ставка НДС Rest Controller",
        description = "Взаимодействие со ставкой НДС")
public class ValueAddedTaxController {

    private final ValueAddedTaxService valueAddedTaxService;

    public ValueAddedTaxController(ValueAddedTaxService valueAddedTaxService) {
        this.valueAddedTaxService = valueAddedTaxService;
    }

    @GetMapping("/taxes")
    @ApiOperation("Получение всех ставок ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа ValueAddedTaxDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<ValueAddedTaxDTO>> findAll() {
        List<ValueAddedTaxDTO> list = valueAddedTaxService.findAll();
        return ResponseEntity.ok(list);
    }

    @GetMapping("taxes/{id}")
    @ApiOperation("Получение ставки НДС по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение ValueAddedTaxDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<ValueAddedTaxDTO> findById(@ApiParam(name = "id",
            value = "Id определенного ValueAddedTaxDTO",
            required = true) @PathVariable("id") Long id) {
        ValueAddedTaxDTO valueAddedTaxDTO = valueAddedTaxService.findById(id);
        return ResponseEntity.ok(valueAddedTaxDTO);
    }

    @PostMapping("/new_tax")
    @ApiOperation("Создание новой ставки НДС")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание ValueAddedTaxDTO",
                    response = ValueAddedTaxDTO.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<ValueAddedTaxDTO> create(@ApiParam(name = "ValueAddedTaxDTO", value = "Объект ValueAddedTaxDTO для создания",
            required = true) @RequestBody ValueAddedTaxDTO valueAddedTaxDTO) {
        valueAddedTaxService.save(valueAddedTaxDTO);
        return ResponseEntity.ok(valueAddedTaxDTO);
    }

    @PutMapping("/taxes")
    @ApiOperation("Редактирование ставки НДС")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление ValueAddedTaxDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<ValueAddedTaxDTO> update(@ApiParam(name = "ValueAddedTaxDTO", value = "Объект ValueAddedTaxDTO для обновления",
            required = true) @RequestBody ValueAddedTaxDTO valueAddedTaxDTO) {
        valueAddedTaxService.save(valueAddedTaxDTO);
        return ResponseEntity.ok(valueAddedTaxDTO);
    }

    @DeleteMapping("/taxes/{id}")
    @ApiOperation("Удаление ставки НДС")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление ValueAddedTaxDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<Long> delete(@ApiParam(name = "id", value = "Id ValueAddedTaxDTO для удаления",
            required = true) @PathVariable("id") Long id) {
        valueAddedTaxService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}

