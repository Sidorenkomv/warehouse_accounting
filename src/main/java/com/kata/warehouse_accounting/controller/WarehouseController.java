package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.WarehouseDTO;
import com.kata.warehouse_accounting.service.WarehouseService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api/warehouse")
@Api(tags = {"Warehouse Rest Controller"})
@Tag(name = "Warehouse Rest Controller",
        description = "API for any warehouses actions")
public class WarehouseController {
    private final WarehouseService warehouseService;

    @Autowired
    public WarehouseController(WarehouseService warehouseService) {
        this.warehouseService = warehouseService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/warehouses")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа WarehouseDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<WarehouseDTO>> findAll() {
        return ResponseEntity.ok(warehouseService.getAll());
    }

    @GetMapping("warehouses/{id}")
    @ApiOperation("Получение записи по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение WarehouseDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<WarehouseDTO> findById(@ApiParam(name = "id", value = "Id нужного WarehouseDTO", required = true)
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(warehouseService.getById(id));
    }

    @ApiOperation("Добавление записи")
    @PostMapping("/warehouses")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание WarehouseDTO",
                    response = WarehouseDTO.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "WarehouseDTO", value = "Объект WarehouseDTO для создания",
            required = true) @RequestBody WarehouseDTO warehouseDTO
    ) {
        warehouseService.create(warehouseDTO);
        return ResponseEntity.ok(warehouseDTO);
    }

    @PutMapping("/warehouses/{id}")
    @ApiOperation("Редактирование записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление WarehouseDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "WarehouseDTO", value = "Объект WarehouseDTO для обновления",
            required = true) @PathVariable("id") Long id,
                                    @RequestBody WarehouseDTO warehouseDTO
    ) {
        warehouseService.update(warehouseDTO);
        return ResponseEntity.ok(warehouseDTO);
    }

    @DeleteMapping("/warehouses/{id}")
    @ApiOperation("Удаление записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление WarehouseDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "Id WarehouseDTO для удаления", required = true)
            @PathVariable("id") Long id
    ) {
        warehouseService.deleteById(id);
        return ResponseEntity.ok(id);
    }

}
