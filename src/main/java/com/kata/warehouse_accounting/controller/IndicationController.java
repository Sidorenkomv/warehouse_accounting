package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.IndicationDTO;
import com.kata.warehouse_accounting.service.IndicationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/indication")
@Api(tags = {"Indication Rest Controller"})
@Tag(name = "Indication Rest Controller",
        description = "API for any indications actions")
public class IndicationController {
    private final IndicationService indicationService;

    @Autowired
    public IndicationController(IndicationService indicationService) {
        this.indicationService = indicationService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/indications")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа IndicationDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<IndicationDTO>> findAll() {
        return ResponseEntity.ok(indicationService.getAll());
    }

    @GetMapping("indications/{id}")
    @ApiOperation("Получение записи по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение IndicationDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<IndicationDTO> findById(@ApiParam(name = "id", value = "Id нужного IndicationDTO", required = true)
                                               @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(indicationService.getById(id));
    }

    @ApiOperation("Добавление записи")
    @PostMapping("/indications")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание IndicationDTO",
                    response = IndicationDTO.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "IndicationDTO", value = "Объект IndicationDTO для создания",
            required = true) @RequestBody IndicationDTO indicationDTO
    ) {
        indicationService.create(indicationDTO);
        return ResponseEntity.ok(indicationDTO);
    }

    @PutMapping("/indications/{id}")
    @ApiOperation("Редактирование записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление IndicationDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "IndicationDTO", value = "Объект IndicationDTO для обновления",
            required = true) @PathVariable("id") Long id,
                                    @RequestBody IndicationDTO indicationDTO
    ) {
        indicationService.update(indicationDTO);
        return ResponseEntity.ok(indicationDTO);
    }

    @DeleteMapping("/indications/{id}")
    @ApiOperation("Удаление записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление IndicationDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "Id IndicationDTO для удаления", required = true)
                                    @PathVariable("id") Long id
    ) {
        indicationService.deleteById(id);
        return ResponseEntity.ok(id);
    }

}
