package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.ProductVarietyDTO;
import com.kata.warehouse_accounting.service.ProductVarietyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/product_variety")
@Api(tags = {"Тип продукции Rest Controller"})
@Tag(name = "Тип продукции Rest Controller",
        description = "Взаимодействие с моделью Товар-тип продукции")
public class ProductVarietyController {
    private final ProductVarietyService productVarietyService;

    public ProductVarietyController(ProductVarietyService productVarietyService) {
        this.productVarietyService = productVarietyService;
    }

    @GetMapping("/list")
    @ApiOperation("Получение всех типов продукции")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка ProductVarietyDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<ProductVarietyDTO>> findAll() {
        List<ProductVarietyDTO> list = productVarietyService.findAll();
        return ResponseEntity.ok(list);
    }

    @GetMapping("list/{id}")
    @ApiOperation("Получение типа продукции по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение ProductVarietyDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<ProductVarietyDTO> findById(@ApiParam(name = "id",
            value = "Id определенного ProductVarietyDTO",
            required = true) @PathVariable("id") Long id) {
        ProductVarietyDTO productVarietyDTO = productVarietyService.findById(id);
        return ResponseEntity.ok(productVarietyDTO);
    }

    @PostMapping("/new_pv")
    @ApiOperation("Создание нового типа продукции")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание ProductVarietyDTO",
                    response = ProductVarietyDTO.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<ProductVarietyDTO> create(@ApiParam(name = "ProductVarietyDTO", value = "Объект ProductVarietyDTO для создания",
            required = true) @RequestBody ProductVarietyDTO productVarietyDTO) {
        productVarietyService.save(productVarietyDTO);
        return ResponseEntity.ok(productVarietyDTO);
    }

    @PutMapping("/update")
    @ApiOperation("Редактирование типа продукции")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление ProductVarietyDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<ProductVarietyDTO> update(@ApiParam(name = "ProductVarietyDTO", value = "Объект ProductVarietyDTO для обновления",
            required = true) @RequestBody ProductVarietyDTO productVarietyDTO) {
        productVarietyService.save(productVarietyDTO);
        return ResponseEntity.ok(productVarietyDTO);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("Удаление типа продукции")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление ProductVarietyDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<Long> delete(@ApiParam(name = "id", value = "Id ProductVarietyDTO для удаления",
            required = true) @PathVariable("id") Long id) {
        productVarietyService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
