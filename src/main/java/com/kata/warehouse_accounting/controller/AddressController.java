package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.AddressDTO;
import com.kata.warehouse_accounting.service.AddressService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api/address")
@Api(tags = {"Address Rest Controller"})
@Tag(name = "Address Rest Controller",
        description = "API for any address actions")
public class AddressController {
    private final AddressService addressService;

    @Autowired
    public AddressController(AddressService addressService) {
        this.addressService = addressService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа AddressDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<AddressDTO>> findAll() {
        return ResponseEntity.ok(addressService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение записи по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение AddressDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<AddressDTO> findById(@ApiParam(name = "id", value = "Id нужного AddressDTO", required = true)
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(addressService.getById(id));
    }

    @ApiOperation("Добавление записи")
    @PostMapping("/")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание AddressDTO",
                    response = AddressDTO.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "AddressDTO", value = "Объект AddressDTO для создания",
            required = true) @RequestBody AddressDTO addressDTO
    ) {
        addressService.create(addressDTO);
        return ResponseEntity.ok(addressDTO);
    }

    @PutMapping("/")
    @ApiOperation("Редактирование записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление AddressDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "AddressDTO",
            value = "Объект AddressDTO для обновления",
            required = true) @RequestBody AddressDTO addressDTO
    ) {
        addressService.update(addressDTO);
        return ResponseEntity.ok(addressDTO);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Удаление записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление AddressDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> delete(@ApiParam(name = "id",
            value = "Id AddressDTO для удаления",
            required = true)
            @PathVariable("id") Long id
    ) {
        addressService.deleteById(id);
        return ResponseEntity.ok(id);
    }

}
