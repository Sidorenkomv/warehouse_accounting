package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.PaymentAccountDTO;
import com.kata.warehouse_accounting.service.PaymentAccountService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/payment_account")
@Api(tags = {"Расчетный счет Rest Controller"})
@Tag(name = "Расчетный Rest Controller",
        description = "Взаимодействие с расчетным счетом")
public class PaymentAccountRestController {
    private final PaymentAccountService paymentAccountService;

    public PaymentAccountRestController(PaymentAccountService paymentAccountService) {
        this.paymentAccountService = paymentAccountService;
    }

    @ApiOperation("Получение списка всех расчетных счетов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа PaymentAccountDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/all")
    public ResponseEntity<List<PaymentAccountDTO>> findAll() {
        List<PaymentAccountDTO> listOfPaymentAccountDTO = paymentAccountService.findAll();
        return ResponseEntity.ok(listOfPaymentAccountDTO);
    }


    @ApiOperation("Получение рачетного счета по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение PaymentAccountDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/{id}")
    public ResponseEntity<PaymentAccountDTO> findById(@ApiParam(name = "id",
            value = "Id определенного PaymentAccountDTO",
            required = true) @PathVariable Long id) {
        PaymentAccountDTO paymentAccountDTO = paymentAccountService.findById(id);
        return ResponseEntity.ok(paymentAccountDTO);
    }

    @ApiOperation("Создание нового рачетного счета")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание PaymentAccountDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PostMapping
    public ResponseEntity<PaymentAccountDTO> create(@ApiParam(name = "PaymentAccountDTO",
            value = "Объект PaymentAccountDTO для создания",
            required = true) @RequestBody PaymentAccountDTO paymentAccountDTO) {
        paymentAccountService.save(paymentAccountDTO);
        return ResponseEntity.ok(paymentAccountDTO);
    }

    @ApiOperation("Редактирование расчетного счета")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление PaymentAccountDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PutMapping
    public ResponseEntity<PaymentAccountDTO> update(@ApiParam(name = "PaymentAccountDTO",
            value = "Объект ChannelSalesDTO для обновления",
            required = true) @RequestBody PaymentAccountDTO paymentAccountDTO) {
        paymentAccountService.save(paymentAccountDTO);
        return ResponseEntity.ok(paymentAccountDTO);
    }

    @ApiOperation("Удаление расчетного счета")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление PaymentAccountDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@ApiParam(name = "id",
            value = "Id PaymentAccountDTO для удаления",
            required = true) @PathVariable Long id) {
        paymentAccountService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
