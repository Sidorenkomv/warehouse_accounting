package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.ContactPersonDTO;
import com.kata.warehouse_accounting.service.ContactPersonService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/contact_person")
@Api(tags = {"Контактное лицо Rest Controller"})
@Tag(name = "Контактное лицо Rest Controller",
        description = "Взаимодействие с контактным лицом")
public class ContactPersonRestController {
    private final ContactPersonService contactPersonService;

    public ContactPersonRestController(ContactPersonService contactPersonService) {
        this.contactPersonService = contactPersonService;
    }

    @ApiOperation("Получение списка всех контактных лиц")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа ContactPersonDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/all")
    public ResponseEntity<List<ContactPersonDTO>> findAll() {
        List<ContactPersonDTO> listOfContactPersonDTO = contactPersonService.findAll();
        return ResponseEntity.ok(listOfContactPersonDTO);
    }

    @ApiOperation("Получение контактного лица по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение ContactPersonDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/{id}")
    public ResponseEntity<ContactPersonDTO> findById(@ApiParam(name = "id",
            value = "Id определенного ContactPersonDTO",
            required = true) @PathVariable Long id) {
        ContactPersonDTO contactPersonDTO = contactPersonService.findById(id);
        return ResponseEntity.ok(contactPersonDTO);
    }

    @ApiOperation("Создание новое контактное лицо")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание ContactPersonDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PostMapping
    public ResponseEntity<ContactPersonDTO> create(@ApiParam(name = "ContactPersonDTO",
            value = "Объект ContactPersonDTO для создания",
            required = true) @RequestBody ContactPersonDTO contactPersonDTO) {
        contactPersonService.save(contactPersonDTO);
        return ResponseEntity.ok(contactPersonDTO);
    }

    @ApiOperation("Редактирование контактного лица")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление ContactPersonDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PutMapping
    public ResponseEntity<ContactPersonDTO> update(@ApiParam(name = "ContactPersonDTO",
            value = "Объект ChannelSalesDTO для обновления",
            required = true) @RequestBody ContactPersonDTO contactPersonDTO) {
        contactPersonService.save(contactPersonDTO);
        return ResponseEntity.ok(contactPersonDTO);
    }

    @ApiOperation("Удаление контактного лица")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление ContactPersonDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@ApiParam(name = "id",
            value = "Id ContactPersonDTO для удаления",
            required = true) @PathVariable Long id) {
        contactPersonService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
