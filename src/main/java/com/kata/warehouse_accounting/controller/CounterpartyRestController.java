package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.CounterpartyRequestDTO;
import com.kata.warehouse_accounting.model.dto.CounterpartyResponseDTO;
import com.kata.warehouse_accounting.service.CounterpartyService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/counterparty")
@Api(tags = {"Контрагент Rest Controller"})
@Tag(name = "Контрагент Rest Controller",
        description = "Взаимодействие с Контрагентом")
public class CounterpartyRestController {
    private final CounterpartyService counterpartyService;

    public CounterpartyRestController(CounterpartyService counterpartyService) {
        this.counterpartyService = counterpartyService;
    }

    @ApiOperation("Получение списка всех контрагентов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа CounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/all")
    public ResponseEntity<List<CounterpartyResponseDTO>> findAll() {
        List<CounterpartyResponseDTO> listOfCounterpartyResponseDTO = counterpartyService.findAll();
        return ResponseEntity.ok(listOfCounterpartyResponseDTO);
    }


    @ApiOperation("Получение контрагента по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение CounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/{id}")
    public ResponseEntity<CounterpartyResponseDTO> findById(@ApiParam(name = "id",
            value = "Id определенного ChannelSalesDTO",
            required = true) @PathVariable Long id) {
        CounterpartyResponseDTO counterpartyResponseDTO = counterpartyService.findById(id);
        return ResponseEntity.ok(counterpartyResponseDTO);
    }

    @ApiOperation("Создание нового контрагента")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание CounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PostMapping
    public ResponseEntity<CounterpartyRequestDTO> create(@ApiParam(name = "CounterpartyDTO",
            value = "Объект CounterpartyDTO для создания",
            required = true) @RequestBody CounterpartyRequestDTO counterpartyRequestDTO) {
        counterpartyService.save(counterpartyRequestDTO);
        return ResponseEntity.ok(counterpartyRequestDTO);
    }

    @ApiOperation("Редактирование контрагента")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление CounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PutMapping
    public ResponseEntity<CounterpartyRequestDTO> update(@ApiParam(name = "CounterpartyDTO",
            value = "Объект CounterpartyDTO для обновления",
            required = true) @RequestBody CounterpartyRequestDTO counterpartyRequestDTO) {
        counterpartyService.save(counterpartyRequestDTO);
        return ResponseEntity.ok(counterpartyRequestDTO);
    }

    @ApiOperation("Удаление контрагента")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление CounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@ApiParam(name = "id",
            value = "Id CounterpartyDTO для удаления",
            required = true) @PathVariable Long id) {
        counterpartyService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
