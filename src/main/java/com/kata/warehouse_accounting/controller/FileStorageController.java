package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.support.FileStorage;
import com.kata.warehouse_accounting.service.FileStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/files")
// Allowed origin of the file
@CrossOrigin("http://localhost:3601")
@Api(tags = {"ContractorType Rest Controller"})
@Tag(name = "ContractorType Rest Controller",
        description = "API for any contractor type actions")
public class FileStorageController {

    FileStorageService fileStorageService;

    @Autowired
    public FileStorageController(FileStorageService fileStorageService) {
        this.fileStorageService = fileStorageService;
    }


//    @DeleteMapping
//    @ApiOperation(value = "Deletes all the files",
//            notes = "Returns boolean")
//    @ApiResponses(value = {
//            @ApiResponse(responseCode = "200", description = "Successfully deleted all uploaded files"),
//            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
//            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
//            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
//    })
//    public ResponseEntity<Boolean> deleteAll(
//    ) {
//        fileStorageService.deleteAll();
//        return ResponseEntity.ok(true);
//    }

    @PostMapping("/upload")
    @ApiOperation(value = "Uploads a new file",
            notes = "Returns MultipartFile")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully uploaded the file"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<MultipartFile> upload(@RequestParam("file") MultipartFile file) {
        fileStorageService.save(file);
        return ResponseEntity.ok(file);
    }

//    @GetMapping
//    @ApiOperation(value = "Returns all the files uploaded",
//            notes = "Returns List of FileStorageDTO (stores urls)")
//    @ApiResponses(value = {
//            @ApiResponse(responseCode = "200", description = "Successfully got the list"),
//            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
//            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
//            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
//    })
//    public ResponseEntity<List<FileStorage>> findAll() {
//        List<FileStorage> fileStorages = fileStorageService.loadAll()
//                .map(path -> {
//                    String name = path.getFileName().toString();
//                    String url = MvcUriComponentsBuilder
//                            .fromMethodName(FileStorageController.class, "getFile",
//                                    path.getFileName().toString())
//                            .build()
//                            .toString();
//
//                    return new FileStorage(name, url);
//                }).collect(Collectors.toList());
//
//                return ResponseEntity.ok(fileStorages);
//    }

    @GetMapping("/{id}")
    @ResponseBody
    @ApiOperation(value = "Returns file with exact ID",
            notes = "Returns Resource")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got the file"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<Resource> findByName(
            @ApiParam(name = "id",
            value = "ID of the file to be searched for",
            example = "1")
            @PathVariable("id") Long id) {
        Resource file = fileStorageService.find(id);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\"" +
                                file.getFilename())
                .body(file);
    }
}
