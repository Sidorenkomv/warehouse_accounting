package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.ProductDetailsDTO;
import com.kata.warehouse_accounting.model.dto.ProductVarietyDTO;
import com.kata.warehouse_accounting.service.ProductDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/product_details")
@Api(tags = {"Детали продукции Rest Controller"})
@Tag(name = "Детали продукции Rest Controller",
        description = "Взаимодействие с ProductDetailsDTO")
public class ProductDetailsController {
    private final ProductDetailsService productDetailsService;

    public ProductDetailsController(ProductDetailsService productDetailsService) {
        this.productDetailsService = productDetailsService;
    }

    @GetMapping("/list")
    @ApiOperation("Получение списка ProductDetailsDTO")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка ProductDetailsDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<ProductDetailsDTO>> findAll() {
        List<ProductDetailsDTO> list = productDetailsService.findAll();
        return ResponseEntity.ok(list);
    }

    @GetMapping("list/{id}")
    @ApiOperation("Получение ProductDetailsDTO по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение ProductDetailsDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<ProductDetailsDTO> findById(@ApiParam(name = "id",
            value = "Id определенного ProductVarietyDTO",
            required = true) @PathVariable("id") Long id) {
        ProductDetailsDTO productDetailsDTO = productDetailsService.findById(id);
        return ResponseEntity.ok(productDetailsDTO);
    }

    @PostMapping("/new_pd")
    @ApiOperation("Создание ProductDetailsDTO")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание ProductDetailsDTO",
                    response = ProductVarietyDTO.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<ProductDetailsDTO> create(@ApiParam(name = "ProductDetailsDTO", value = "Объект ProductDetailsDTO для создания",
            required = true) @RequestBody ProductDetailsDTO productDetailsDTO) {
        productDetailsService.save(productDetailsDTO);
        return ResponseEntity.ok(productDetailsDTO);
    }

    @PutMapping("/update")
    @ApiOperation("Редактирование ProductDetailsDTO")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление ProductDetailsDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<ProductDetailsDTO> update(@ApiParam(name = "ProductDetailsDTO", value = "Объект ProductDetailsDTO для обновления",
            required = true) @RequestBody ProductDetailsDTO productDetailsDTO) {
        productDetailsService.save(productDetailsDTO);
        return ResponseEntity.ok(productDetailsDTO);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("Удаление ProductDetailsDTO")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление ProductDetailsDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<Long> delete(@ApiParam(name = "id", value = "Id ProductDetailsDTO для удаления",
            required = true) @PathVariable("id") Long id) {
        productDetailsService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
