package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.CurrencyDTO;
import com.kata.warehouse_accounting.service.CurrencyService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api/currency")
@Api(tags = {"Currency Rest Controller"})
@Tag(name = "Currency Rest Controller",
        description = "API for any currency actions")
public class CurrencyController {
    private final CurrencyService currencyService;

    @Autowired
    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа CurrencyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<CurrencyDTO>> findAll() {
        return ResponseEntity.ok(currencyService.getAll());
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение записи по ID")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение CurrencyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<CurrencyDTO> findById(
            @ApiParam(name = "id",
                    value = "ID записи для получения",
                    required = true)
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(currencyService.getById(id));
    }

    @ApiOperation("Добавление записи")
    @PostMapping("/")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание CurrencyDTO",
                    response = CurrencyDTO.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(
            @ApiParam(name = "CurrencyDTO",
                    value = "Объект CurrencyDTO для создания",
                    required = true)
            @RequestBody CurrencyDTO currencyDTO
    ) {
        currencyService.create(currencyDTO);
        return ResponseEntity.ok(currencyDTO);
    }

    @PutMapping("/{id}")
    @ApiOperation("Редактирование записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление CurrencyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(
            @ApiParam(name = "CurrencyDTO",
                    value = "Объект CurrencyDTO для обновления",
                    required = true)
            @PathVariable("id") Long id,
            @RequestBody CurrencyDTO currencyDTO
    ) {
        currencyService.update(currencyDTO);
        return ResponseEntity.ok(currencyDTO);
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Удаление записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление CurrencyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> delete(
            @ApiParam(name = "id",
                    value = "ID CurrencyDTO для удаления",
                    required = true)
            @PathVariable("id") Long id
    ) {
        currencyService.deleteById(id);
        return ResponseEntity.ok(id);
    }


}
