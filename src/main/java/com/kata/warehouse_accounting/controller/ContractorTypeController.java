package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.ContractorTypeDTO;
import com.kata.warehouse_accounting.model.dto.LegalDetailsDTO;
import com.kata.warehouse_accounting.service.ContractorTypeService;
import com.kata.warehouse_accounting.service.LegalDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/contractor_types")
@Api(tags = {"ContractorType Rest Controller"})
@Tag(name = "ContractorType Rest Controller",
        description = "API for any contractor type actions")
public class ContractorTypeController {

    private final ContractorTypeService contractorTypeService;

    @Autowired
    public ContractorTypeController(ContractorTypeService contractorTypeService) {
        this.contractorTypeService = contractorTypeService;
    }

    @GetMapping
    @ApiOperation(value = "Returns all the contractor type that exist",
            notes = "Returns a list of ContractorTypeDTO",
            response = ContractorTypeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got a list of contractor type"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<List<ContractorTypeDTO>> findAll() {
        List<ContractorTypeDTO> listOfContractorTypeDTO = contractorTypeService.findAll(false);
        return ResponseEntity.ok(listOfContractorTypeDTO);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Returns contractor type with exact ID",
            notes = "Returns ContractorTypeDTO",
            response = ContractorTypeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got contractor type"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<ContractorTypeDTO> findById(
            @ApiParam(name = "id",
                    value = "Value of object ID that is to be searched for",
                    example = "1")
            @PathVariable("id") Long id
    ) {
        ContractorTypeDTO contractorTypeDTO = contractorTypeService.findById(id);
        return ResponseEntity.ok(contractorTypeDTO);
    }

    @PostMapping
    @ApiOperation(value = "Creates new contractor type",
            notes = "Returns the body of ContractorTypeDTO",
            response = ContractorTypeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully created contractor type"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<ContractorTypeDTO> create(
            @ApiParam(name = "ContractorTypeDTO",
                    value = "The body of object that is to be created")
            @RequestBody ContractorTypeDTO contractorTypeDTO
    ) {
        contractorTypeService.save(contractorTypeDTO);
        return ResponseEntity.ok(contractorTypeDTO);
    }

    @PutMapping
    @ApiOperation(value = "Updates contractor type",
            notes = "Returns the body of ContractorTypeDTO",
            response = ContractorTypeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated contractor type"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<ContractorTypeDTO> update(
            @ApiParam(name = "ContractorTypeDTO",
                    value = "The body of object that is to be updated")
            @RequestBody ContractorTypeDTO contractorTypeDTO
    ) {
        contractorTypeService.save(contractorTypeDTO);
        return ResponseEntity.ok(contractorTypeDTO);
    }

    @PostMapping("/{id}")
    @ApiOperation(value = "Deletes contractor type with exact ID",
            notes = "Returns ID of the deleted contractor type",
            response = ContractorTypeDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully deleted contractor type"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<Long> delete(
            @ApiParam(name = "id",
                    value = "Value of object ID that is to be deleted",
                    example = "1")
            @PathVariable("id") Long id
    ) {
        contractorTypeService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
