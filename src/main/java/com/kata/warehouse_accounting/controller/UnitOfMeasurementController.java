package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.UnitMeasurementDTO;
import com.kata.warehouse_accounting.service.UnitOfMeasurementService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/unitOfMeasurement")
@Api(tags = {"UnitOfMeasurement Rest Controller"})
@Tag(name = "UnitOfMeasurement Rest Controller",
        description = "API for any unitOfMeasurement actions")
public class UnitOfMeasurementController {
    private final UnitOfMeasurementService unitOfMeasurementService;

    @Autowired
    public UnitOfMeasurementController(UnitOfMeasurementService unitOfMeasurementService) {
        this.unitOfMeasurementService = unitOfMeasurementService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/unitOfMeasurements")
    public ResponseEntity<List<UnitMeasurementDTO>> findAll() {
        return ResponseEntity.ok(unitOfMeasurementService.getAll());
    }

    @GetMapping("unitOfMeasurements/{id}")
    @ApiOperation("Получение записи по id")
    public ResponseEntity<UnitMeasurementDTO> findById(
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(unitOfMeasurementService.getById(id));
    }

    @ApiOperation("Добавление записи")
    @PostMapping("/unitOfMeasurements")
    public ResponseEntity<?> create(
            @RequestBody UnitMeasurementDTO unitMeasurementDTO
    ) {
        unitOfMeasurementService.create(unitMeasurementDTO);
        return ResponseEntity.ok(unitMeasurementDTO);
    }

    @PutMapping("/unitOfMeasurements/{id}")
    @ApiOperation("Редактирование записи")
    public ResponseEntity<?> update(@PathVariable("id") Long id,
                                    @RequestBody UnitMeasurementDTO unitMeasurementDTO
    ) {
        unitOfMeasurementService.update(unitMeasurementDTO);
        return ResponseEntity.ok(unitMeasurementDTO);
    }

    @DeleteMapping("/unitOfMeasurements/{id}")
    @ApiOperation("Удаление записи")
    public ResponseEntity<?> delete(
            @PathVariable("id") Long id
    ) {
        unitOfMeasurementService.deleteById(id);
        return ResponseEntity.ok(id);
    }


}
