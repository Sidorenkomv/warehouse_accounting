package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.BarCode;
import com.kata.warehouse_accounting.model.dto.BarCodeDTO;
import com.kata.warehouse_accounting.model.generators.BarbecueBarcodeGenerator;
import com.kata.warehouse_accounting.service.BarCodeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.awt.image.BufferedImage;
import java.util.List;

@RestController
@RequestMapping("/barcodes")
@Api(tags = {"Штрихкод Rest Controller"})
@Tag(name = "Штрихкод Rest Controller",
        description = "Взаимодействие со штрихкодом")
public class BarCodeController {
    private final BarCodeService barCodeService;

    public BarCodeController(BarCodeService barCodeService) {
        this.barCodeService = barCodeService;
    }

    @GetMapping(value = "/barbecue/ean13/{barcode}", produces = MediaType.IMAGE_PNG_VALUE)
    @ApiOperation("Генерация EAN13")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешная генерация"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<BufferedImage> barbecueEAN13Barcode(@PathVariable("barcode") String barcode)
            throws Exception {
        return ResponseEntity.ok(BarbecueBarcodeGenerator.generateEAN13BarcodeImage(barcode));
    }

    @GetMapping(value = "/barbecue/ean8/{barcode}", produces = MediaType.IMAGE_PNG_VALUE)
    @ApiOperation("Генерация EAN8")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа ValueAddedTaxDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<BufferedImage> barbecueEAN8Barcode(@PathVariable("barcode") String barcode)
            throws Exception {
        return ResponseEntity.ok(BarbecueBarcodeGenerator.generateEAN8BarcodeImage(barcode));
    }

    @GetMapping(value = "/barbecue/code128/{barcode}", produces = MediaType.IMAGE_PNG_VALUE)
    @ApiOperation("Генерация Code128")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа ValueAddedTaxDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<BufferedImage> barbecueCode128Barcode(@PathVariable("barcode") String barcode) throws Exception {
        return ResponseEntity.ok(BarbecueBarcodeGenerator.generateCode128BarcodeImage(barcode));
    }

    @GetMapping(value = "/barbecue/upc/{barcode}", produces = MediaType.IMAGE_PNG_VALUE)
    @ApiOperation("Генерация UPC")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа ValueAddedTaxDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<BufferedImage> barbecueUpcBarcode(@PathVariable("barcode") String barcode) throws Exception {
        return ResponseEntity.ok(BarbecueBarcodeGenerator.generateUPCABarcodeImage(barcode));
    }

    @PostMapping("/new_barcode")
    @ApiOperation("Создание EAN13")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание",
                    response = BarCode.class),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<BarCodeDTO> create(@ApiParam(name = "BarCodeDTO", value = "Объект BarCodeDTO для создания",
            required = true) @RequestBody BarCodeDTO barCodeDTO) {
        barCodeService.save(barCodeDTO);
        return ResponseEntity.ok(barCodeDTO);
    }

    @GetMapping("/list")
    @ApiOperation("Получение списка штрихкодов ")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка BarCodeDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<BarCodeDTO>> findAll() {
        List<BarCodeDTO> list = barCodeService.findAll();
        return ResponseEntity.ok(list);
    }

    @GetMapping("/list/{id}")
    @ApiOperation("Получение штрихкода по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение BarCodeDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<BarCodeDTO> findById(@ApiParam(name = "id",
            value = "Id определенного штрихкода",
            required = true) @PathVariable("id") Long id) {
        BarCodeDTO barCodeDTO = barCodeService.findById(id);
        return ResponseEntity.ok(barCodeDTO);
    }

    @PutMapping("/update")
    @ApiOperation("Редактирование штрихкода")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление BarCodeDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<BarCodeDTO> update(@ApiParam(name = "BarCodeDTO", value = "Объект BarCodeDTO для обновления",
            required = true) @RequestBody BarCodeDTO barCodeDTO) {
        barCodeService.save(barCodeDTO);
        return ResponseEntity.ok(barCodeDTO);
    }

    @DeleteMapping("/delete/{id}")
    @ApiOperation("Удаление штрихкода")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление BarCodeDTO"),
            @ApiResponse(code = 404, message = "Данный контроллер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<Long> delete(@ApiParam(name = "id", value = "Id BarCodeDTO для удаления",
            required = true) @PathVariable("id") Long id) {
        barCodeService.deleteById(id);
        return ResponseEntity.ok(id);
    }

}
