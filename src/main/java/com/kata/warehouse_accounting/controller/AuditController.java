package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.AuditDTO;
import com.kata.warehouse_accounting.service.AuditService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/audit")
@Api(tags = {"Audit Rest Controller"})
@Tag(name = "Audit Rest Controller",
        description = "Api for audit actions")
public class AuditController {
    private final AuditService auditService;

    public AuditController(AuditService auditService) {
        this.auditService = auditService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/audit")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа AuditDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<AuditDTO>> findAll() {
        return ResponseEntity.ok(auditService.getAll());
    }

    @GetMapping("audit/{id}")
    @ApiOperation("Получение записи по ip")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение AuditDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<AuditDTO> findById(@ApiParam(name = "id", value = "Id нужного AuditDTO", required = true)
    @PathVariable("id") Long id) {
        return ResponseEntity.ok(auditService.getById(id));
    }
}
