package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.ContractorTypeDTO;
import com.kata.warehouse_accounting.model.dto.TaxSystemDTO;
import com.kata.warehouse_accounting.service.ContractorTypeService;
import com.kata.warehouse_accounting.service.TaxSystemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/tax_system")
@Api(tags = {"TaxSystem Rest Controller"})
@Tag(name = "TaxSystem Rest Controller",
        description = "API for any tax system actions")
public class TaxSystemController {

    private final TaxSystemService taxSystemService;

    @Autowired
    public TaxSystemController(TaxSystemService taxSystemService) {
        this.taxSystemService = taxSystemService;
    }

    @GetMapping
    @ApiOperation(value = "Returns all the tax systems that exist",
            notes = "Returns a list of TaxSystemDTO",
            response = TaxSystemDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got a list of contractor type"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<List<TaxSystemDTO>> findAll() {
        List<TaxSystemDTO> listOfTaxSystemsDTO = taxSystemService.findAll(false);
        return ResponseEntity.ok(listOfTaxSystemsDTO);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Returns tax system type with exact ID",
            notes = "Returns TaxSystemDTO",
            response = TaxSystemDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got tax system"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<TaxSystemDTO> findById(
            @ApiParam(name = "id",
                    value = "Value of object ID that is to be searched for",
                    example = "1")
            @PathVariable("id") Long id
    ) {
        TaxSystemDTO taxSystemDTO = taxSystemService.findById(id);
        return ResponseEntity.ok(taxSystemDTO);
    }

    @PostMapping
    @ApiOperation(value = "Creates new tax system",
            notes = "Returns the body of TaxSystemDTO",
            response = TaxSystemDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully created tax system"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<TaxSystemDTO> create(
            @ApiParam(name = "TaxSystemDTO",
                    value = "The body of object that is to be created")
            @RequestBody TaxSystemDTO taxSystemDTO
    ) {
        taxSystemService.save(taxSystemDTO);
        return ResponseEntity.ok(taxSystemDTO);
    }

    @PutMapping
    @ApiOperation(value = "Updates tax system",
            notes = "Returns the body of TaxSystemDTO",
            response = TaxSystemDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated tax system"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<TaxSystemDTO> update(
            @ApiParam(name = "TaxSystemDTO",
                    value = "The body of object that is to be updated")
            @RequestBody TaxSystemDTO taxSystemDTO
    ) {
        taxSystemService.save(taxSystemDTO);
        return ResponseEntity.ok(taxSystemDTO);
    }

    @PostMapping("/{id}")
    @ApiOperation(value = "Deletes tax system with exact ID",
            notes = "Returns ID of the deleted tax system",
            response = TaxSystemDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully deleted tax system"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<Long> delete(
            @ApiParam(name = "id",
                    value = "Value of object ID that is to be deleted",
                    example = "1")
            @PathVariable("id") Long id
    ) {
        taxSystemService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
