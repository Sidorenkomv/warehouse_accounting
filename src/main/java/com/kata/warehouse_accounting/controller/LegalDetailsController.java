package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.LegalDetailsDTO;
import com.kata.warehouse_accounting.service.LegalDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/legal_details")
@Api(tags = {"LegalDetails Rest Controller"})
@Tag(name = "LegalDetails Rest Controller",
        description = "API for any legal details actions")
public class LegalDetailsController {

    private final LegalDetailsService legalDetailsService;

    @Autowired
    public LegalDetailsController(LegalDetailsService legalDetailsService) {
        this.legalDetailsService = legalDetailsService;
    }

    @GetMapping
    @ApiOperation(value = "Returns all the legal details that exist",
            notes = "Returns a list of LegalDetailsDTO",
            response = LegalDetailsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got a list of legal details"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<List<LegalDetailsDTO>> findAll() {
        List<LegalDetailsDTO> listOfLegalDetailsDTO = legalDetailsService
                .findAll(false);
        return ResponseEntity.ok(listOfLegalDetailsDTO);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Returns legal details with exact ID",
            notes = "Returns LegalDetailsDTO",
            response = LegalDetailsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got legal details"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<LegalDetailsDTO> findById(
            @ApiParam(name = "id",
                    value = "Value of object ID that is to be searched for",
                    example = "1")
            @PathVariable("id") Long id
    ) {
        LegalDetailsDTO legalDetailsDTO = legalDetailsService.findById(id);
        return ResponseEntity.ok(legalDetailsDTO);
    }

    @PostMapping
    @ApiOperation(value = "Creates new legal details",
            notes = "Returns the body of LegalDetailsDTO",
            response = LegalDetailsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully created legal details"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<LegalDetailsDTO> create(
            @ApiParam(name = "LegalDetailsDTO",
                    value = "The body of object that is to be created")
            @RequestBody LegalDetailsDTO legalDetailDto
    ) {
        legalDetailsService.save(legalDetailDto);
        return ResponseEntity.ok(legalDetailDto);
    }

    @PutMapping
    @ApiOperation(value = "Updates legal details",
            notes = "Returns the body of LegalDetailsDTO",
            response = LegalDetailsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated legal details"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<LegalDetailsDTO> update(
            @ApiParam(name = "LegalDetailsDTO",
                    value = "The body of object that is to be updated")
            @RequestBody LegalDetailsDTO legalDetailDto
    ) {
        legalDetailsService.save(legalDetailDto);
        return ResponseEntity.ok(legalDetailDto);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deletes legal details with exact ID",
            notes = "Returns ID of the deleted legal details",
            response = LegalDetailsDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully deleted legal details"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<Long> delete(
            @ApiParam(name = "id",
                    value = "Value of object ID that is to be deleted",
                    example = "1",
                    required = true)
            @PathVariable("id") Long id
    ) {
        legalDetailsService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
