package com.kata.warehouse_accounting.controller;


import com.kata.warehouse_accounting.model.dto.GoodsGroupDTO;
import com.kata.warehouse_accounting.service.GoodsGroupService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


@RestController
@RequestMapping("/goods")
@Api(tags = {"GoodsGroup Rest Controller"})
@Tag(name = "GoodsGroup Rest Controller", description = "API for GoodsGroup actions")
public class GoodsGroupController {
    GoodsGroupService goodsGroupService;

    @Autowired
    public GoodsGroupController(GoodsGroupService goodsGroupService) {
        this.goodsGroupService = goodsGroupService;

    }

    @ApiOperation(value = "Получение списка групп товаров",
            notes = "Возвращает список групп",
            response = GoodsGroupDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Групп товаров"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/groups")
    public ResponseEntity<List<GoodsGroupDTO>> showAllGroups() {
        return new ResponseEntity<>(goodsGroupService.showAllGroups(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение группы товаров по id",
            notes = "Возвращает группу по ID",
            response = GoodsGroupDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка Группы товаров"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/this_group/{id}")
    public ResponseEntity<GoodsGroupDTO> showGroup(@ApiParam(name = "id", value = "Значение объекта по предоставленному ID")
                                                       @PathVariable Long id) {
        try {
            GoodsGroupDTO goodsGroup = goodsGroupService.showGroupById(id);
            return new ResponseEntity<>(goodsGroup, HttpStatus.FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Добавление новой группы товаров",
            notes = "Возвращает группу по ID",
            response = GoodsGroupDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное добавление Группы товаров"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PostMapping("/add_newGroup")
    public ResponseEntity<GoodsGroupDTO> addNewGroup(@ApiParam(name = "id", value = "Тело объекта, который необходимо создать")
                                                         @RequestBody GoodsGroupDTO goodsGroup) {
        try {
            goodsGroupService.addGroup(goodsGroup);
        return new ResponseEntity<>(goodsGroup, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Редактирование группы товаров",
            notes = "Возвращает группу по ID",
            response = GoodsGroupDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное редактирование Группы товаров"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PutMapping("/edit_group")
    public ResponseEntity<GoodsGroupDTO> editGroup(@ApiParam(name = "id", value = "Тело объекта, который необходимо отредактировать")
                                                       @RequestBody GoodsGroupDTO goodsGroup) {
        try {
            goodsGroupService.editGroup(goodsGroup);
            return new ResponseEntity<>(goodsGroup, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Удаление группы товаров",
            notes = "Возвращает ID удаленной группы",
            response = GoodsGroupDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление Группы товаров"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @DeleteMapping("/delete_group/{id}")
    public ResponseEntity<GoodsGroupDTO> delete(@ApiParam(name = "id", value = "Тело объекта, который необходимо удалить")
                                                    @PathVariable Long id) {
        try {
            goodsGroupService.deleteGroup(id);
            return new ResponseEntity<>(HttpStatus.GONE);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

}
