package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.GroupsCounterpartyDTO;
import com.kata.warehouse_accounting.service.GroupsCounterpartyService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/groups-counterparty")
@Api(tags = {"Группы контрагента Rest Controller"})
@Tag(name = "Группы контрагента Rest Controller",
        description = "Взаимодействие с группами контрагента")
public class GroupsCounterpartyRestController {
    private final GroupsCounterpartyService groupsCounterpartyService;

    public GroupsCounterpartyRestController(GroupsCounterpartyService groupsCounterpartyService) {
        this.groupsCounterpartyService = groupsCounterpartyService;
    }

    @ApiOperation("Получение списка всех групп контрагента")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа GroupsCounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/all")
    public ResponseEntity<List<GroupsCounterpartyDTO>> findAll() {
        List<GroupsCounterpartyDTO> listOfGroupsCounterpartyDTO = groupsCounterpartyService.findAll();
        return ResponseEntity.ok(listOfGroupsCounterpartyDTO);
    }

    @ApiOperation("Получение группы контрагента по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение GroupsCounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/{id}")
    public ResponseEntity<GroupsCounterpartyDTO> findById(@ApiParam(name = "id",
            value = "Id определенного ContactPersonDTO",
            required = true) @PathVariable Long id) {
        GroupsCounterpartyDTO groupsCounterpartyDTO = groupsCounterpartyService.findById(id);
        return ResponseEntity.ok(groupsCounterpartyDTO);
    }

    @ApiOperation("Создание новое группы контрагента")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание GroupsCounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PostMapping
    public ResponseEntity<GroupsCounterpartyDTO> create(@ApiParam(name = "GroupsCounterpartyDTO",
            value = "Объект GroupsCounterpartyDTO для создания",
            required = true) @RequestBody GroupsCounterpartyDTO groupsCounterpartyDTO) {
        groupsCounterpartyService.save(groupsCounterpartyDTO);
        return ResponseEntity.ok(groupsCounterpartyDTO);
    }

    @ApiOperation("Редактирование группы контрагента")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление GroupsCounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PutMapping
    public ResponseEntity<GroupsCounterpartyDTO> update(@ApiParam(name = "GroupsCounterpartyDTO",
            value = "Объект GroupsCounterpartyDTO для обновления",
            required = true) @RequestBody GroupsCounterpartyDTO groupsCounterpartyDTO) {
        groupsCounterpartyService.save(groupsCounterpartyDTO);
        return ResponseEntity.ok(groupsCounterpartyDTO);
    }

    @ApiOperation("Удаление группы контрагента")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление GroupsCounterpartyDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@ApiParam(name = "id",
            value = "Id GroupsCounterpartyDTO для удаления",
            required = true) @PathVariable Long id) {
        groupsCounterpartyService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
