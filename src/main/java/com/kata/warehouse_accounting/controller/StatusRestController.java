package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.StatusDTO;
import com.kata.warehouse_accounting.service.StatusService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/status")
@Api(tags = {"Статус Rest Controller"})
@Tag(name = "Статус Rest Controller",
        description = "Взаимодействие со статусом")
public class StatusRestController {

    private final StatusService statusService;

    public StatusRestController(StatusService statusService) {
        this.statusService = statusService;
    }


    @ApiOperation("Получение списка всех статусов")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа StatusDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/all")
    public ResponseEntity<List<StatusDTO>> findAll() {
        List<StatusDTO> listOfStatusDTO = statusService.findAll();
        return ResponseEntity.ok(listOfStatusDTO);
    }

    @ApiOperation("Создание нового статуса")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание StatusDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PostMapping
    public ResponseEntity<StatusDTO> create(@ApiParam(name = "StatusDTO",
            value = "Объект StatusDTO для создания",
            required = true) @RequestBody StatusDTO statusDTO) {
        statusService.save(statusDTO);
        return ResponseEntity.ok(statusDTO);
    }

    @ApiOperation("Редактирование статуса")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление StatusDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PutMapping
    public ResponseEntity<StatusDTO> update(@ApiParam(name = "StatusDTO",
            value = "Объект StatusDTO для обновления",
            required = true) @RequestBody StatusDTO statusDTO) {
        statusService.save(statusDTO);
        return ResponseEntity.ok(statusDTO);
    }

    @ApiOperation("Удаление канала продаж")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление StatusDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@ApiParam(name = "id",
            value = "Id StatusDTO для удаления",
            required = true) @PathVariable Long id) {
        statusService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
