package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.AccountDTO;
import com.kata.warehouse_accounting.service.AccountService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api/account")
@Api(tags = {"Account Rest Controller"})
@Tag(name = "Account Rest Controller",
        description = "API for any accounts actions")
public class AccountController {
    private final AccountService accountService;

    @Autowired
    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/accounts")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа AccountDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<AccountDTO>> findAll() {
        return ResponseEntity.ok(accountService.getAll());
    }

    @GetMapping("accounts/{id}")
    @ApiOperation("Получение записи по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение AccountDto"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<AccountDTO> findById(@ApiParam(name = "id", value = "Id нужного AccountDTO", required = true)
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(accountService.getById(id));
    }

    @ApiOperation("Добавление записи")
    @PostMapping("/accounts")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание AccountDTO",
                    response = AccountDTO.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "AccountDTO", value = "Объект AccountDTO для создания",
            required = true) @RequestBody AccountDTO accountDTO
    ) {
        accountService.create(accountDTO);
        return ResponseEntity.ok(accountDTO);
    }

    @PutMapping("/accounts/{id}")
    @ApiOperation("Редактирование записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление AccountDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "AccountDTO", value = "Объект AccountDTO для обновления",
            required = true) @PathVariable("id") Long id,
                                    @RequestBody AccountDTO accountDTO
    ) {
        accountService.update(accountDTO);
        return ResponseEntity.ok(accountDTO);
    }

    @DeleteMapping("/accounts/{id}")
    @ApiOperation("Удаление записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление AccountDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "Id AccountDTO для удаления", required = true)
            @PathVariable("id") Long id
    ) {
        accountService.deleteById(id);
        return ResponseEntity.ok(id);
    }

}
