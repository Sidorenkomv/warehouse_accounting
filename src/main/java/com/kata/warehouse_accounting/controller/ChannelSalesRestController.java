package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.ChannelSalesDTO;
import com.kata.warehouse_accounting.service.ChannelSalesService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/channel_sales")
@Api(tags = {"Канал продаж Rest Controller"})
@Tag(name = "Канал продаж Rest Controller",
        description = "Взаимодействие с каналом продаж")
public class ChannelSalesRestController {

    private final ChannelSalesService channelSalesService;

    public ChannelSalesRestController(ChannelSalesService channelSalesService) {
        this.channelSalesService = channelSalesService;
    }

    @ApiOperation("Получение списка всех каналов продаж")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа ChannelSalesDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/all")
    public ResponseEntity<List<ChannelSalesDTO>> findAll() {
        List<ChannelSalesDTO> listOfChannelSalesDTO = channelSalesService.findAll();
        return ResponseEntity.ok(listOfChannelSalesDTO);
    }


    @ApiOperation("Получение канала продаж по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение ChannelSalesDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/{id}")
    public ResponseEntity<ChannelSalesDTO> findById(@ApiParam(name = "id",
            value = "Id определенного ChannelSalesDTO",
            required = true) @PathVariable Long id) {
        ChannelSalesDTO channelSalesDTO = channelSalesService.findById(id);
        return ResponseEntity.ok(channelSalesDTO);
    }

    @ApiOperation("Создание нового канала продаж")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание ChannelSalesDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PostMapping
    public ResponseEntity<ChannelSalesDTO> create(@ApiParam(name = "ChannelSalesDTO",
            value = "Объект ChannelSalesDTO для создания",
            required = true) @RequestBody ChannelSalesDTO channelSalesDTO) {
        channelSalesService.save(channelSalesDTO);
        return ResponseEntity.ok(channelSalesDTO);
    }

    @ApiOperation("Редактирование канала продаж")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление ChannelSalesDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PutMapping
    public ResponseEntity<ChannelSalesDTO> update(@ApiParam(name = "ChannelSalesDTO",
            value = "Объект ChannelSalesDTO для обновления",
            required = true) @RequestBody ChannelSalesDTO channelSalesDTO) {
        channelSalesService.save(channelSalesDTO);
        return ResponseEntity.ok(channelSalesDTO);
    }

    @ApiOperation("Удаление канала продаж")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление ChannelSalesDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @DeleteMapping("/{id}")
    public ResponseEntity<Long> delete(@ApiParam(name = "id",
            value = "Id ChannelSalesDTO для удаления",
            required = true) @PathVariable Long id) {
        channelSalesService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}