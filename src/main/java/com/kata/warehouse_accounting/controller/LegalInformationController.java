package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.LegalInformationDTO;
import com.kata.warehouse_accounting.service.FileStorageService;
import com.kata.warehouse_accounting.service.LegalInformationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/legal_information")
@Api(tags = {"LegalInformation Rest Controller"})
@Tag(name = "LegalInformation Rest Controller",
        description = "API for any legal information actions")
public class LegalInformationController {

    private final LegalInformationService legalInformationService;
    private final FileStorageService fileStorageService;

    @Autowired
    public LegalInformationController(LegalInformationService legalInformationService,
                                      FileStorageService fileStorageService) {
        this.legalInformationService = legalInformationService;
        this.fileStorageService = fileStorageService;
    }

    @GetMapping
    @ApiOperation(value = "Returns all the legal information on companies that exist",
            notes = "Returns a list of LegalDetailsDTO",
            response = LegalInformationDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got a list of legal details"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<List<LegalInformationDTO>> findAll() {
        List<LegalInformationDTO> listOfLegalInformationDTO =
                legalInformationService.findAll(false);
        return ResponseEntity.ok(listOfLegalInformationDTO);
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Returns legal information of exact company by ID",
            notes = "Returns LegalDetailsDTO",
            response = LegalInformationDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully got legal information of the company"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<LegalInformationDTO> findById(
            @ApiParam(name = "id",
                    value = "Value of object ID that is to be searched for",
                    example = "1")
            @PathVariable("id") Long id
    ) {
        LegalInformationDTO legalInformationDTO = legalInformationService.findById(id);
        return ResponseEntity.ok(legalInformationDTO);
    }

    @PostMapping
    @ApiOperation(value = "Creates new company",
            notes = "Returns the body of LegalInformationDTO",
            response = LegalInformationDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully created legal details"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<LegalInformationDTO> create(
            @ApiParam(name = "LegalDetailsDTO",
                    value = "The body of object that is to be created")
            @RequestBody LegalInformationDTO legalInformationDTO,
            @RequestParam MultipartFile file
            ) throws IOException {
        legalInformationService.save(legalInformationDTO);
        fileStorageService.save(file);
        return ResponseEntity.ok(legalInformationDTO);
    }

    @PutMapping
    @ApiOperation(value = "Updates existing legal information of a company",
            notes = "Returns the body of LegalInformationDTO",
            response = LegalInformationDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully updated legal information of the company"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<LegalInformationDTO> update(
            @ApiParam(name = "LegalDetailsDTO",
                    value = "The body of object that is to be updated")
            @RequestBody LegalInformationDTO legalInformationDTO
    ) {
        legalInformationService.save(legalInformationDTO);
        return ResponseEntity.ok(legalInformationDTO);
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Deletes legal information of the company by ID",
            notes = "Returns ID of the deleted legal information of the company",
            response = LegalInformationDTO.class)
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successfully deleted company's information"),
            @ApiResponse(responseCode = "401", description = "Do not have access to this operation"),
            @ApiResponse(responseCode = "403", description = "Operation is forbidden"),
            @ApiResponse(responseCode = "404", description = "This controlled was not found"),
    })
    public ResponseEntity<Long> delete(
            @ApiParam(name = "id",
                    value = "Value of object ID that is to be deleted",
                    example = "1",
                    required = true)
            @PathVariable("id") Long id
    ) {
        legalInformationService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
