package com.kata.warehouse_accounting.controller;


import com.kata.warehouse_accounting.model.dto.GoodDTO;
import com.kata.warehouse_accounting.service.GoodService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/good")
@Api(tags = {"Good Rest Controller"})
@Tag(name = "Good Rest Controller", description = "API for Goods actions")
public class GoodController {

    GoodService goodService;

    @Autowired
    public GoodController(GoodService goodService) {
        this.goodService = goodService;
    }

    @ApiOperation(value = "Получение списка всех товаров",
            notes = "Возвращает список групп",
            response = GoodDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение списка товаров"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/goods")
    public ResponseEntity<List<GoodDTO>> showAllGroups() {
        return new ResponseEntity<>(goodService.showAll(), HttpStatus.OK);
    }

    @ApiOperation(value = "Получение товара по id",
            notes = "Возвращает список групп",
            response = GoodDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение товара по id"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @GetMapping("/this_good/{id}")
    public ResponseEntity<GoodDTO> showGroup(@ApiParam(name = "id", value = "Значение объекта по предоставленному ID")
                                                 @PathVariable Long id) {
        try {
            GoodDTO goodDTO = goodService.showById(id);
            return new ResponseEntity<>(goodDTO, HttpStatus.FOUND);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @ApiOperation(value = "Добавление нового товара",
            notes = "Возвращает список групп",
            response = GoodDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное добавление товара"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PostMapping("/add_newGood")
    public ResponseEntity<GoodDTO> addNewGroup(@ApiParam(name = "id", value = "Тело объекта, который необходимо создать")
                                                   @RequestBody GoodDTO goodDTO) {
        try {
            goodService.addGood(goodDTO);
            return new ResponseEntity<>(goodDTO, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Редактирование товара",
            notes = "Возвращает список групп",
            response = GoodDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное редактирование товара"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @PutMapping("/edit_good")
    public ResponseEntity<GoodDTO> editGroup(@ApiParam(name = "id", value = "Тело объекта, который необходимо отредактировать")
                                                 @RequestBody GoodDTO goodDTO) {
        try {
            goodService.editGood(goodDTO);
            return new ResponseEntity<>(goodDTO, HttpStatus.ACCEPTED);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @ApiOperation(value = "Удаление товара",
            notes = "Возвращает список групп",
            response = GoodDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление товара"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    @DeleteMapping("/delete_good/{id}")
    public ResponseEntity<GoodDTO> delete(@ApiParam(name = "id", value = "Тело объекта, который необходимо удалить")
                                              @PathVariable Long id) {
        try {
            goodService.deleteGood(id);
            return new ResponseEntity<>(HttpStatus.GONE);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }
}
