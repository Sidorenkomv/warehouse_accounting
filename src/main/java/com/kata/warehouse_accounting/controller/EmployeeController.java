package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.EmployeeDTO;
import com.kata.warehouse_accounting.service.EmployeeService;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@RequestMapping("/api/employees")
@Api(tags = {"Employees Rest Controller"})
@Tag(name = "Employees Rest Controller",
        description = "API for any employees actions")
public class EmployeeController {
    private final EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/employees")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа EmployeeDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<EmployeeDTO>> findAll() {
        return ResponseEntity.ok(employeeService.getAll());
    }

    @GetMapping("employees/{id}")
    @ApiOperation("Получение записи по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение EmployeeDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<EmployeeDTO> findById(@ApiParam(name = "id", value = "Id нужного EmployeeDTO", required = true)
            @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(employeeService.getById(id));
    }

    @ApiOperation("Добавление записи")
    @PostMapping("/employees")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание EmployeeDTO",
                    response = EmployeeDTO.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "EmployeeDTO", value = "Объект EmployeeDTO для создания",
            required = true)  @RequestBody EmployeeDTO employeeDTO
    ) {
        employeeService.create(employeeDTO);
        return ResponseEntity.ok(employeeDTO);
    }

    @PutMapping("/employees/{id}")
    @ApiOperation("Редактирование записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление EmployeeDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "EmployeeDTO", value = "Объект EmployeeDTO для обновления",
            required = true) @PathVariable("id") Long id,
                                    @RequestBody EmployeeDTO employeeDTO
    ) {
        employeeService.update(employeeDTO);
        return ResponseEntity.ok(employeeDTO);
    }

    @DeleteMapping("/employees/{id}")
    @ApiOperation("Удаление записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление EmployeeDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "Id EmployeeDTO для удаления", required = true)
            @PathVariable("id") Long id
    ) {
        employeeService.deleteById(id);
        return ResponseEntity.ok(id);
    }

}
