package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.DocumentDTO;
import com.kata.warehouse_accounting.service.DocumentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/document")
@Api(tags = {"Document Rest Controller"})
@Tag(name = "Document Rest Controller",
        description = "API for any documents actions")
public class DocumentController {
    private final DocumentService documentService;

    @Autowired
    public DocumentController(DocumentService documentService) {
        this.documentService = documentService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/documents")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа DocumentDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<DocumentDTO>> findAll() {
        return ResponseEntity.ok(documentService.getAll());
    }

    @GetMapping("documents/{id}")
    @ApiOperation("Получение записи по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение DocumentDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<DocumentDTO> findById(@ApiParam(name = "id", value = "Id нужного DocumentDTO", required = true)
                                              @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(documentService.getById(id));
    }

    @ApiOperation("Добавление записи")
    @PostMapping("/documents")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание DocumentDTO",
                    response = DocumentDTO.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "DocumentDTO", value = "Объект DocumentDTO для создания",
            required = true) @RequestBody DocumentDTO documentDTO
    ) {
        documentService.create(documentDTO);
        return ResponseEntity.ok(documentDTO);
    }

    @PutMapping("/documents/{id}")
    @ApiOperation("Редактирование записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление DocumentDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "DocumentDTO", value = "Объект DocumentDTO для обновления",
            required = true) @PathVariable("id") Long id,
                                    @RequestBody DocumentDTO documentDTO
    ) {
        documentService.update(documentDTO);
        return ResponseEntity.ok(documentDTO);
    }

    @DeleteMapping("/documents/{id}")
    @ApiOperation("Удаление записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление DocumentDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "Id DocumentDTO для удаления", required = true)
                                    @PathVariable("id") Long id
    ) {
        documentService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
