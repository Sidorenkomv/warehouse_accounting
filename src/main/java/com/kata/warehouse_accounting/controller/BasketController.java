package com.kata.warehouse_accounting.controller;

import com.kata.warehouse_accounting.model.dto.BasketDTO;
import com.kata.warehouse_accounting.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import io.swagger.annotations.*;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/basket")
@Api(tags = {"Basket Rest Controller"})
@Tag(name = "Basket Rest Controller",
        description = "API for any baskets actions")
public class BasketController {
    private final BasketService basketService;

    @Autowired
    public BasketController(BasketService basketService) {
        this.basketService = basketService;
    }

    @ApiOperation("Получение списка всех записей")
    @GetMapping("/baskets")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение листа BasketDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<List<BasketDTO>> findAll() {
        return ResponseEntity.ok(basketService.getAll());
    }

    @GetMapping("baskets/{id}")
    @ApiOperation("Получение записи по id")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное получение BasketDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<BasketDTO> findById(@ApiParam(name = "id", value = "Id нужного BasketDTO", required = true)
                                               @PathVariable("id") Long id
    ) {
        return ResponseEntity.ok(basketService.getById(id));
    }

    @ApiOperation("Добавление записи")
    @PostMapping("/baskets")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное создание BasketDTO",
                    response = BasketDTO.class),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> create(@ApiParam(name = "BasketDTO", value = "Объект BasketDTO для создания",
            required = true) @RequestBody BasketDTO basketDTO
    ) {
        basketService.create(basketDTO);
        return ResponseEntity.ok(basketDTO);
    }

    @PutMapping("/baskets/{id}")
    @ApiOperation("Редактирование записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное обновление BasketDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> update(@ApiParam(name = "BasketDTO", value = "Объект BasketDTO для обновления",
            required = true) @PathVariable("id") Long id,
                                    @RequestBody BasketDTO basketDTO
    ) {
        basketService.update(basketDTO);
        return ResponseEntity.ok(basketDTO);
    }

    @DeleteMapping("/baskets/{id}")
    @ApiOperation("Удаление записи")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Успешное удаление BasketDTO"),
            @ApiResponse(code = 404, message = "Данный контролер не найден"),
            @ApiResponse(code = 403, message = "Операция запрещена"),
            @ApiResponse(code = 401, message = "Нет доступа к данной операции")})
    public ResponseEntity<?> delete(@ApiParam(name = "id", value = "Id BasketDTO для удаления", required = true)
                                    @PathVariable("id") Long id
    ) {
        basketService.deleteById(id);
        return ResponseEntity.ok(id);
    }
}
