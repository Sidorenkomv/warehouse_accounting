package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;
    @Column
    private Boolean entrance;
    @Column
    private String lastName;
    @Column
    private String picture;
    @Column
    private String firstName;
    @Column
    private String patronymic;
    @Column
    private String email;
    @Column
    private Long telephone;
    @Column
    private String post;
    @Column
    private String inn;
    @Column
    private String login;
    @Column
    private String description;
    @Column
    private String role;
    @Column
    private Boolean sharedAccess;
    @Column
    private String ownerDepartment;
    @Column
    private String ownerEmployee;
    @Column
    private LocalDate whenChanged;
    @Column
    private String whoChanged;
}
