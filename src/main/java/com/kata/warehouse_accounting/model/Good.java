package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;

@Getter
@Setter
@ToString
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "goods")
@SQLDelete(sql = "UPDATE goods SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class Good {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    /** Товар - true, услуга - false */
    @Column(name = "is_good")
    private boolean category;

    /* Описание */
    private String description;

    /** Группа товаров */
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "good_group_id")
    private GoodsGroup goodsGroup;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    private Country country;

    /* Поставщик */
    private String supplier;

    /* Артикул */
    @Column(name = "vendor_code")
    private String vendorCode;

    /* Код */
    private String code;

    /* Внешний код */
    private String externalCode;

    @ManyToOne
    @JoinColumn(name = "unit_of_measurement_id")
    private UnitOfMeasurement unitOfMeasurement;

    /* Вес */
    private Double weight;

    /* Объем */
    private Double volume;

    @ManyToOne
    @JoinColumn(name = "value_added_tax_id")
    private ValueAddedTax valueAddedTax;

    @ManyToOne
    @JoinColumn(name = "tax_system_id")
    private TaxSystem taxSystem;

    private boolean deleted = Boolean.FALSE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        Good good = (Good) o;
        return id != null && Objects.equals(id, good.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}