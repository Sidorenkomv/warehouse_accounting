package com.kata.warehouse_accounting.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@ToString
@AllArgsConstructor
@Table(name = "country")
@SQLDelete(sql = "UPDATE country SET deleted = true WHERE id = ?")
@Where(clause = "deleted = false")
public class Country {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String shortName;

    private String longName;

    private Long digitalCode;

    private String literalCode;

    private boolean deleted = Boolean.FALSE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return deleted == country.deleted && Objects.equals(id, country.id) && Objects.equals(shortName, country.shortName) && Objects.equals(longName, country.longName) && Objects.equals(digitalCode, country.digitalCode) && Objects.equals(literalCode, country.literalCode);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, shortName, longName, digitalCode, literalCode, deleted);
    }
}
