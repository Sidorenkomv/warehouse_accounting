package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "groups_counterparty")
@SQLDelete(sql = "UPDATE groupsCounterparty SET deleted = true WHERE id=?")
@Where(clause = "deleted = false")
public class GroupsCounterparty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;
}
