package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "currency")
@SQLDelete(sql = "UPDATE currency SET deleted = true WHERE id = ?")
@Where(clause = "deleted = false")
@FilterDef(name = "deletedCurrencyFilter",
        parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedCurrencyFilter",
        condition = "deleted = :isDeleted")
public class Currency {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Boolean accountingCurrency;
    private String name;
    private String longName;
    private Long digitalCode;
    private String letterCode;
    private Double exchangeRate;
    private Boolean deleted = Boolean.FALSE;
}
