package com.kata.warehouse_accounting.model;


import com.kata.warehouse_accounting.model.dto.UnitMeasurementDTO;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "purchases")
@SQLDelete(sql = "UPDATE purchases SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class Purchase {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

// toDO    ImageDto imageDTO;


    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_of_measurement_id")
    private Set<UnitOfMeasurement> unitMeasurementDTO = new HashSet<>();

    /** Принято*/
    private BigDecimal received;

    /** Доступно*/
    private BigDecimal enabled;

    /** Остаток*/
    private BigDecimal balance;

    /** Резерв*/
    private BigDecimal reserve;

    /** Ожидание*/
    private BigDecimal expectation;

    /** Вес*/
    private BigDecimal weight;

    /** Объем*/
    private BigDecimal volume;

    /** Сумма НДС*/
    private BigDecimal sumTax;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;



}
