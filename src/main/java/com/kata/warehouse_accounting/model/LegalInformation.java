package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;

/**
 * This is the model that will contain all the legal information on companies.
 * */
@Entity
@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name = "legal_information")
@SQLDelete(sql = "UPDATE legal_information SET deleted = true WHERE id = ?")
@Where(clause = "deleted = false")
@FilterDef(name = "deletedLegalInformationFilter",
        parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedLegalInformationFilter",
        condition = "deleted = :isDeleted")
public class LegalInformation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    private String code;
    private String phone;
    private String fax;
    private String email;
    @Column(name = "payer_vat")
    private boolean taxpayer;
    @OneToOne
    @JoinColumn(name = "address_id")
    private Address actualAddress;
    @Column(name = "address_comment")
    private String addressComment;
    private String comment;
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "legal_details_id")
    private LegalDetails legalDetails;
    private String directorName;
    private String directorTitle;
    /*
     * Files should be uploaded to the database in order to be
     * accessible at any time without the need to upload them ones again */
    @OneToOne
    @JoinColumn(name = "director_signature_id")
    private FileObject directorSignature;
    private String accountantName;
    @OneToOne
    @JoinColumn(name = "accountant_signature_id")
    private FileObject accountantSignature;
    @OneToOne
    @JoinColumn(name = "stamp_id")
    private FileObject stamp;
    @OneToOne
    @JoinColumn(name = "logo_id")
    private FileObject logo;
    private Boolean deleted = Boolean.FALSE;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
        LegalInformation that = (LegalInformation) o;
        return id != null && Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return getClass().hashCode();
    }
}

