package com.kata.warehouse_accounting.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {
    private Long id;
    private Boolean entrance;
    private String lastName;
    private String picture;
    private String firstName;
    private String patronymic;
    private String email;
    private Long telephone;
    private String post;
    private String inn;
    private String login;
    private String description;
    private String role;
    private Boolean sharedAccess;
    private String ownerDepartment;
    private String ownerEmployee;
    private LocalDate whenChanged;
    private String whoChanged;
}
