package com.kata.warehouse_accounting.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UnitMeasurementDTO {
    private Long id;
    private Boolean type;
    private String shortName;
    private String fullName;
    private Long digitalCode;
    private Boolean sharedAccess;
}
