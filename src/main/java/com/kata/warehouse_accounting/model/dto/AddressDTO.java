package com.kata.warehouse_accounting.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AddressDTO {
    private Long id;
    private Long index;
    private CountryDTO country;
    private String region;
    private String city;
    private String street;
    private String house;
    private String office;
    private String other;
    private String comment;
    private Boolean deleted = Boolean.FALSE;
}
