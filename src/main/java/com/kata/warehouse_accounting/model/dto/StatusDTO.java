package com.kata.warehouse_accounting.model.dto;

import com.kata.warehouse_accounting.model.support.StatusColor;
import com.kata.warehouse_accounting.model.support.StatusType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сущность статуса контрагента")
public class StatusDTO {
    @Schema(description = "Индентификатор")
    private Long id;

    @Schema(description = "намиенование")
    private String name;

    @Schema(description = "Тип статуса")
    private StatusType statusType;

    @Schema(description = "Цвет статуса")
    private StatusColor statusColor;
}
