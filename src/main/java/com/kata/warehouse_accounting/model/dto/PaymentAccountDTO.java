package com.kata.warehouse_accounting.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сущность расчетного счета")
public class PaymentAccountDTO {
    @Schema(description = "Индентификатор")
    private Long id;

    @Schema(description = "БИК")
    private String bic;

    @Schema(description = "Банк")
    private String bank;

    @Schema(description = "Адрес")
    private String address;

    @Schema(description = "Корр. счет")
    private String corAccount;

    @Schema(description = "Расчетный счет")
    private String payAccount;
}
