package com.kata.warehouse_accounting.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сущность ставки НДС")
public class ValueAddedTaxDTO {

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Тип")
    private String type;

    @Schema(description = "Ставка НДС")
    private byte vatRate;

    @Schema(description = "Комментарий")
    private String comment;

    @Schema(description = "Владелец-сотрудник")
    private Long employeeId;

    @Schema(description = "Владелец-отдел")
    private String ownerDepartment;

    @Schema(description = "Кто изменил")
    private Long whoChangedIt;

    @Schema(description = "Когда изменён")
    private LocalDateTime dateOfChange;

}


