package com.kata.warehouse_accounting.model.dto;

import com.kata.warehouse_accounting.model.ProductDetails;
import com.kata.warehouse_accounting.model.support.ProductType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сущность типа продукции")
public class ProductVarietyDTO {

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Тип продукции")
    private ProductType type;

    @Schema(description = "Детали продукции")
    private ProductDetails productDetails;

}
