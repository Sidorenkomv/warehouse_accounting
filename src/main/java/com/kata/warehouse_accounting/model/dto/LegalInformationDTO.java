package com.kata.warehouse_accounting.model.dto;

import com.kata.warehouse_accounting.model.LegalDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LegalInformationDTO {
    private Long id;
    private String name;
    private String code;
    private String phone;
    private String fax;
    private String email;
    private boolean taxpayer;
    private AddressDTO actualAddress;
    private String addressComment;
    private String comment;
    private LegalDetails legalDetails;
    private String directorName;
    private String directorTitle;
    private FileObjectDTO directorSignature;
    private String accountantName;
    private FileObjectDTO accountantSignature;
    private FileObjectDTO stamp;
    private FileObjectDTO logo;
    private Boolean deleted = Boolean.FALSE;
}
