package com.kata.warehouse_accounting.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сущность статуса контрагента")
public class ContactPersonDTO {
    @Schema(description = "Индентификатор")
    private Long id;

    @Schema(description = "ФИО")
    private String fullName;

    @Schema(description = "должность")
    private String designation;

    @Schema(description = "телефон")
    private String phone;

    @Schema(description = "Электронный адрес")
    private String email;

    @Schema(description = "комментарий")
    private String comment;
}
