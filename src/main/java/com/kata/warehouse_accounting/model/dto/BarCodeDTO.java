package com.kata.warehouse_accounting.model.dto;

import com.kata.warehouse_accounting.model.support.CodeType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сущность Штрихкод")
public class BarCodeDTO {

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Тип шрихкода")
    private CodeType codeType;

    @Schema(description = "Цифровой код")
    private String codeDigital;
}
