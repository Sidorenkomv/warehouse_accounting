package com.kata.warehouse_accounting.model.dto;


import com.kata.warehouse_accounting.model.GoodsGroup;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;



@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoodsGroupDTO {

    private Long id;

    private String name;

    private String description;

    private Long code;

    private GoodsGroup goodsGroup;

    private String externalCode;
}
