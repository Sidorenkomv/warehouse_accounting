package com.kata.warehouse_accounting.model.dto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BasketDTO {
    private Long id;
    private LocalDate time;
    private BigDecimal amount;
    private Long fromWarehouseId;
    private Long toWarehouseId;
    private Long organizationId;
    private Long counterpartyId;
    private Long statusId;
    private Boolean remove = false;
}
