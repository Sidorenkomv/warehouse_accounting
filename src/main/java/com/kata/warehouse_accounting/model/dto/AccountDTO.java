package com.kata.warehouse_accounting.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AccountDTO {
    private Long id;

    private LocalDate time;

    private Long counterpartyId;

    private Long legalInformationId;

    private Long warehouseId;

    private BigDecimal amount;

    private BigDecimal paidFor;

    private BigDecimal notPaid;

    private BigDecimal accepted;

    private LocalDate planPaymentDate;

    private Long incomingNumber;

    private Boolean sharedAccess;

}
