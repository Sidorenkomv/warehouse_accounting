package com.kata.warehouse_accounting.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;


@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseDto {

    Long id;

// toDO    ImageDto imageDTO;

    private UnitMeasurementDTO unitMeasurementDTO;

    private BigDecimal received;

    private BigDecimal enabled;

    private BigDecimal balance;

    private BigDecimal reserve;

    private BigDecimal expectation;

    private BigDecimal weight;

    private BigDecimal volume;

    private BigDecimal sumTax;

    private boolean deleted;
}
