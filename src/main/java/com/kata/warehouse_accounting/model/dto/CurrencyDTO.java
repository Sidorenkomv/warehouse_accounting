package com.kata.warehouse_accounting.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CurrencyDTO {
    private Long id;
    private Boolean accountingCurrency;
    private String name;
    private String longName;
    private Long digitalCode;
    private String letterCode;
    private Double exchangeRate;
    private Boolean deleted = Boolean.FALSE;
}
