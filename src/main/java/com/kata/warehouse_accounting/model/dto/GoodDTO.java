package com.kata.warehouse_accounting.model.dto;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GoodDTO {


    private Long id;

    /** Товар - true, услуга - false */
    private boolean category;

    /** Описание */
    private String description;

//    /** Группа товаров */
    private GoodsGroupDTO goodsGroup;

//    private Country country;

    /** Поставщик */
    private String supplier;

    /** Артикул*/
    private String vendorCode;

    /** Код */
    private String code;

    /** Внешний код */
    private String externalCode;

    private UnitMeasurementDTO unitOfMeasurement;

    /** Вес */
    private Double weight;

    /** Объем */
    private Double volume;

//    private ValueAddedTax valueAddedTax;

    private boolean deleted;

}
