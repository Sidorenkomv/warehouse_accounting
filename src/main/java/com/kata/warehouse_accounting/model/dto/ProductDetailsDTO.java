package com.kata.warehouse_accounting.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Подробно о типе продукции")
public class ProductDetailsDTO {

    @Schema(description = "Идентификатор")
    private Long id;

    @Schema(description = "Код товарной номенклатуры внешнеэконом деятельности")
    private String commodityNomenclatureOfForeignEconomicActivity;

    @Schema(description = "Целевой пол")
    private String targetGender;

    @Schema(description = "Тип производства")
    private String typeOfProduction;

    @Schema(description = "Возрастная категория")
    private String ageCategory;

    @Schema(description = "Модель")
    private String model;

    @Schema(description = "Комплект")
    private boolean isSet = Boolean.FALSE;

    @Schema(description = "Прослеживаемый")
    private boolean isTraceable = Boolean.FALSE;

    @Schema(description = "Частичная продажа")
    private boolean isPartialSale = Boolean.FALSE;
}
