package com.kata.warehouse_accounting.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AuditDTO {
    private Long id;
    private LocalDateTime time;
    private Long employee;
    private String event;
    private boolean deleted;
}
