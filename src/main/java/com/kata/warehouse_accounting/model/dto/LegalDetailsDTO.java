package com.kata.warehouse_accounting.model.dto;

import com.kata.warehouse_accounting.model.Address;
import com.kata.warehouse_accounting.model.ContractorType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LegalDetailsDTO {
    private Long id;
    private ContractorTypeDTO contractorType;
    private String name;
    private AddressDTO address;
    private String comment;
    private Long inn;
    private Long kpp;
    private Long ogrn;
    private Long okpo;
    private Boolean deleted = Boolean.FALSE;
}
