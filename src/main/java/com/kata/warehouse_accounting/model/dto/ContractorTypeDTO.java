package com.kata.warehouse_accounting.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ContractorTypeDTO {
    private Long id;
    private String name;
    private boolean deleted = Boolean.FALSE;
}
