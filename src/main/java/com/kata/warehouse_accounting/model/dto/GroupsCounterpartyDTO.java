package com.kata.warehouse_accounting.model.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сущность группы контрагента")
public class GroupsCounterpartyDTO {
    @Schema(description = "Индентификатор")
    private Long id;

    @Schema(description = "name")
    private String name;
}
