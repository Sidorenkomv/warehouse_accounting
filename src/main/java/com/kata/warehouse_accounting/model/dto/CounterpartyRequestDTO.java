package com.kata.warehouse_accounting.model.dto;

import com.kata.warehouse_accounting.model.*;
import com.kata.warehouse_accounting.model.support.PricesType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сущность контрагент запрос")
public class CounterpartyRequestDTO {

    @Schema(description = "Индентификатор")
    private Long id;

    @Schema(description = "Статус")
    private Status status;

    @Schema(description = "Группы")
    private Set<GroupsCounterpartyDTO> groupsCounterparty;

    @Schema(description = "Телефон")
    private Long phone;

    @Schema(description = "Факс")
    private Long fax;

    @Schema(description = "Электронный адрес")
    private String email;

    @Schema(description = "Фактический адрес")
    private Address address;


    @Schema(description = "Комментарий к адресу")
    private String commentAddress;

    @Schema(description = "Комментарий")
    private String comment;

    @Schema(description = "Код")
    private String cod;

//    @Schema(description = "Внешний код")
//    private String externalCode;

    @Schema(description = "Контактные лица")
    private List<ContactPerson> contactPersons;

    @Schema(description = "Реквизиты")
    private LegalDetails legalDetails;

    @Schema(description = "Расчетный счет")
    private List<PaymentAccount> paymentAccount;

    @Schema(description = "Цены")
    private PricesType prices;

    @Schema(description = "Номер диск.карты")
    private String discountNumber;

    @Schema(description = "Владелец-сотрудник")
    private Employee ownerEmployee;

    @Schema(description = "Владелец-отдел")
    private String ownerDepartment;

    @Schema(description = "Когда изменён")
    private LocalDateTime dateOfChange;

    @Schema(description = "Кто изменил")
    private Employee whoChangedIt;
}
