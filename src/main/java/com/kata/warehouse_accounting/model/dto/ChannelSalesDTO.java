package com.kata.warehouse_accounting.model.dto;

import com.kata.warehouse_accounting.model.support.SalesType;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Сущность канала продаж")
public class ChannelSalesDTO {
    @Schema(description = "Индентификатор")
    private Long id;

    @Schema(description = "Наименование")
    private String name;

    @Schema(description = "Тип")
    private SalesType salesType;

    @Schema(description = "Описание")
    private String description;

    @Schema(description = "Владелец-сотрудник")
    private Long ownerEmployee;

    @Schema(name = "Владелец-отдел")
    private String ownerDepartment;

    @Schema(description = "Когда изменён")
    private LocalDateTime dateOfChange;

    @Schema(description = "Кто изменил")
    private Long whoChangedIt;
}
