package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

/**
 * This is the model that will contain all the legal types of contractors.
 * */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "type_of_contractor")
@SQLDelete(sql = "UPDATE type_of_contractor SET deleted = true WHERE id = ?")
@Where(clause = "deleted = false")
@FilterDef(name = "deletedLegalContractorType",
        parameters = @ParamDef(name = "isDeleted", type = "boolean"))
@Filter(name = "deletedLegalContractorType",
        condition = "deleted = :isDeleted")
public class ContractorType {
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;
        private String name;
        private boolean deleted;

        @Override
        public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
                ContractorType that = (ContractorType) o;
                return id != null && Objects.equals(id, that.id);
        }

        @Override
        public int hashCode() {
                return getClass().hashCode();
        }
}
