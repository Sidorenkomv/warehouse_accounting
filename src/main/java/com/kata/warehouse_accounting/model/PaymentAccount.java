package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "paymentAccount")
@SQLDelete(sql = "UPDATE paymentAccount SET deleted = true WHERE id=?")
@Where(clause = "deleted = false")
public class PaymentAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "bic")
    private String bic;

    @Column(name = "bank")
    private String bank;

    @Column(name = "address")
    private String address;

    @Column(name = "corAccount")
    private String corAccount;

    @Column(name = "payAccount")
    private String payAccount;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;
}
