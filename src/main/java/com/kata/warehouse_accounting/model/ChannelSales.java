package com.kata.warehouse_accounting.model;

import com.kata.warehouse_accounting.model.support.SalesType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "channel_sales")
@SQLDelete(sql = "UPDATE channel_sales SET deleted = true WHERE id=?")
@Where(clause = "deleted = false")
public class ChannelSales {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "sales_type")
    private SalesType salesType;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee  ownerEmployee;

    @Column(name = "owner_department")
    private String ownerDepartment;

    @Column(name = "date_of_change")
    private LocalDateTime dateOfChange;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "who_changed_it")
    private Employee whoChangedIt;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;
}
