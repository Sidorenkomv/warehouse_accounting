package com.kata.warehouse_accounting.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "product_group")
@SQLDelete(sql = "UPDATE product_group SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class GoodsGroup {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    private String name;

    private String description;

    private Long code;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "product_group_id")
    private GoodsGroup goodsGroup;

    private String externalCode;

    @Column
    private boolean deleted = Boolean.FALSE;


}
