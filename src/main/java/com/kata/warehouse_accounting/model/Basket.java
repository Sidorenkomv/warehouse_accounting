package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.hibernate.annotations.ParamDef;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "basket")
@SQLDelete(sql = "UPDATE basket SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class Basket {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "time")
    private LocalDate time;

    @Column(name = "amount")
    private BigDecimal amount;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warehouse_id", insertable=false, updatable=false)
    private Warehouse fromWarehouse;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warehouse_id", insertable=false, updatable=false)
    private Warehouse toWarehouse;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "legal_information_id", insertable=false, updatable=false)
    private LegalInformation organization;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "counterparty_id", insertable=false, updatable=false)
    private Counterparty counterparty;

    //private String project;

    //private String contract;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "status_id")
    private Status status;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;
}