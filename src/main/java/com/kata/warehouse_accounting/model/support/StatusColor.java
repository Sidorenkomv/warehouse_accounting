package com.kata.warehouse_accounting.model.support;

public enum StatusColor {
    RGB_153_153_153,
    RGB_233_41_25,
    RGB_230_129_22,
    RGB_105_75_30,
    RGB_189_174_0,
    RGB_162_198_23,
    RGB_134_170_96,
    RGB_0_135_57,
    RGB_133_198_222,
    RGB_0_159_227,
    RGB_66_112_129,
    RGB_0_73_154,
    RGB_236_97_159,
    RGB_164_102_189,
    RGB_141_20_48,
    RGB_0_0_0
}
