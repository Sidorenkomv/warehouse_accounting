package com.kata.warehouse_accounting.model.support;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * This is the model that will store uploaded files to our project.
 * <br><br>
 * About: <a href="https://www.bezkoder.com/spring-boot-file-upload/">...</a> */
@Getter
@Setter
@ToString
@NoArgsConstructor
public class FileStorage {
    private String name;
    private String url;

    public FileStorage(String name, String url) {
        this.url = url;
        this.name = name;
    }
}
