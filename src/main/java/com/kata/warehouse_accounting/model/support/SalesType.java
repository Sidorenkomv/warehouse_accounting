package com.kata.warehouse_accounting.model.support;

public enum SalesType {
    MESSENGER,
    SOCIALNETWORK,
    MARKETPLACE,
    ONLINESTORE,
    MESSAGEBOARD,
    DIRECTSALES,
    OTHER
}