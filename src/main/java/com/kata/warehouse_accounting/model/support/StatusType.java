package com.kata.warehouse_accounting.model.support;

public enum StatusType {
    ORDINARY,
    FINAL_POSITIVE,
    FINAL_NEGATIVE
}
