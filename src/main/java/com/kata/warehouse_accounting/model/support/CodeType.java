package com.kata.warehouse_accounting.model.support;

public enum CodeType {
    EAN8,
    EAN13,
    CODE128,
    UPC
}
