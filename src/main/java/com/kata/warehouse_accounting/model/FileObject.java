package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;

/**
 * This is the model that will store uploaded files to our project.
 * <a href="https://www.bezkoder.com/spring-boot-file-upload/">About</a> */
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class FileObject {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    /**
    * We tell JPA our intention of storing a potentially large binary.
    * <a href="https://www.baeldung.com/java-db-storing-files">About</a>*/
    @Lob
    private byte[] content;
    private String url;

    public FileObject(String fileName, String url) {
        this.name = fileName;
        this.url = url;
    }
}
