package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.time.LocalDate;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "account")
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "time")
    private LocalDate time;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "counterparty_id")
    private Counterparty counterparty;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "legal_information_id")
    private LegalInformation legalInformation;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "warehouse_id")
    private Warehouse warehouse;

    @Column(name = "amount")
    private BigDecimal amount;

    @Column(name = "paid_for")
    private BigDecimal paidFor;

    @Column(name = "not_paid")
    private BigDecimal notPaid;

    @Column(name = "accepted")
    private BigDecimal accepted;

    @Column(name = "plan_payment_date")
    private LocalDate planPaymentDate;

    @Column(name = "incoming_number")
    private Long incomingNumber;

    //private String project;

    //private String contract;

    @Column(name = "shared_access")
    private Boolean sharedAccess;

}
