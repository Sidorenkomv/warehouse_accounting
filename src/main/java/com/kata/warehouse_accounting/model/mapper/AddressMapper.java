package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Address;
import com.kata.warehouse_accounting.model.dto.AddressDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AddressMapper {
    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    AddressDTO addressToDTO(Address address);

    Address addressDTOToModel(AddressDTO addressDTO);

}
