package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Audit;
import com.kata.warehouse_accounting.model.dto.AuditDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AuditMapper {
    AuditMapper INSTANCE = Mappers.getMapper(AuditMapper.class);

    @Mapping(source = "employee.id", target = "employee")
    AuditDTO auditToDTO(Audit audit);
    @Mapping(source = "employee", target = "employee.id")
    Audit auditDTOToModel(AuditDTO auditDTO);
}
