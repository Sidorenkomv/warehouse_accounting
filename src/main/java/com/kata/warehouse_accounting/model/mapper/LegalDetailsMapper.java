package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.ContractorType;
import com.kata.warehouse_accounting.model.LegalDetails;
import com.kata.warehouse_accounting.model.dto.ContractorTypeDTO;
import com.kata.warehouse_accounting.model.dto.LegalDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LegalDetailsMapper {

    LegalDetailsMapper INSTANCE = Mappers.getMapper(LegalDetailsMapper.class);

    LegalDetailsDTO legalDetailsToDto(LegalDetails legalDetails);

    LegalDetails legalDetailsToModel(LegalDetailsDTO legalDetailsDto);
}
