package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.GroupsCounterparty;
import com.kata.warehouse_accounting.model.dto.GroupsCounterpartyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GroupsCounterpartyMapper {
    GroupsCounterpartyMapper GROUPS_COUNTERPARTY_MAPPER = Mappers.getMapper(GroupsCounterpartyMapper.class);

    GroupsCounterparty toEntity(GroupsCounterpartyDTO groupsCounterpartyDTO);

    GroupsCounterpartyDTO toResponse(GroupsCounterparty groupsCounterparty);
}
