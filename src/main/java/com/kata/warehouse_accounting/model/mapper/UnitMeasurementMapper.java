package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.dto.UnitMeasurementDTO;
import com.kata.warehouse_accounting.model.UnitOfMeasurement;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UnitMeasurementMapper {
    UnitMeasurementMapper INSTANCE = Mappers.getMapper(UnitMeasurementMapper.class);

    UnitMeasurementDTO unitOfMeasurementToDTO(UnitOfMeasurement unitOfMeasurement);
    UnitOfMeasurement unitOfMeasurementDTOToModel(UnitMeasurementDTO unitMeasurementDTO);

}
