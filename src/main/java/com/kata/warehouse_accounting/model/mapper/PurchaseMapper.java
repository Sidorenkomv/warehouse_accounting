package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Purchase;
import com.kata.warehouse_accounting.model.dto.PurchaseDto;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PurchaseMapper {

    PurchaseMapper INSTANCE = Mappers.getMapper(PurchaseMapper.class);

    Purchase purchaseDtoToModel(PurchaseDto purchaseDto);

    PurchaseDto purchaseToDto(Purchase purchase);
}
