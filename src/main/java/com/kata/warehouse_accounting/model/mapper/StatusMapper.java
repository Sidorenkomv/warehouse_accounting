package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Status;
import com.kata.warehouse_accounting.model.dto.StatusDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface StatusMapper {
    StatusMapper STATUS_MAPPER = Mappers.getMapper(StatusMapper.class);

    Status toEntity(StatusDTO statusDTO);

    StatusDTO toResponse(Status status);
}
