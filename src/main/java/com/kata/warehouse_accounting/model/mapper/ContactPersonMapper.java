package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.ContactPerson;
import com.kata.warehouse_accounting.model.dto.ContactPersonDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ContactPersonMapper {
    ContactPersonMapper CONTACT_PERSON_MAPPER = Mappers.getMapper(ContactPersonMapper.class);

    ContactPerson toEntity(ContactPersonDTO contactPersonDTO);

    ContactPersonDTO toResponse(ContactPerson contactPerson);
}
