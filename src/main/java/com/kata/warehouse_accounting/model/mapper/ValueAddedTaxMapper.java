package com.kata.warehouse_accounting.model.mapper;
import com.kata.warehouse_accounting.model.ValueAddedTax;
import com.kata.warehouse_accounting.model.dto.ValueAddedTaxDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ValueAddedTaxMapper {
    ValueAddedTaxMapper INSTANCE = Mappers.getMapper(ValueAddedTaxMapper.class);

    @Mapping(source = "employeeId", target = "ownerEmployee.id")
    @Mapping(source = "whoChangedIt", target = "whoChangedIt.id")
    ValueAddedTax toModel(ValueAddedTaxDTO vatDto);

    @Mapping(source = "ownerEmployee.id", target = "employeeId")
    @Mapping(source = "whoChangedIt.id", target = "whoChangedIt")
    ValueAddedTaxDTO toDto(ValueAddedTax vat);

}
