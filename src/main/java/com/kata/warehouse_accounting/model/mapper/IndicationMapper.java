package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Indication;
import com.kata.warehouse_accounting.model.dto.IndicationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface IndicationMapper {
    IndicationMapper INSTANCE = Mappers.getMapper(IndicationMapper.class);

    IndicationDTO indicationToDTO(Indication indication);
    Indication indicationDTOToModel(IndicationDTO indicationDTO);
}
