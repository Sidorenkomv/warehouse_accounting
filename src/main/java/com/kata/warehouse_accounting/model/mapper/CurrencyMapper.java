package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Currency;
import com.kata.warehouse_accounting.model.dto.CurrencyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CurrencyMapper {
    CurrencyMapper INSTANCE = Mappers.getMapper(CurrencyMapper.class);

    CurrencyDTO currencyToDTO(Currency currency);
    Currency currencyDTOToModel(CurrencyDTO currencyDTO);
}
