package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.ProductDetails;
import com.kata.warehouse_accounting.model.dto.ProductDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductDetailsMapper {

    ProductDetailsMapper INSTANCE = Mappers.getMapper(ProductDetailsMapper.class);

    ProductDetailsDTO toDto(ProductDetails productDetails);

    ProductDetails toModel(ProductDetailsDTO productDetailsDTO);
}
