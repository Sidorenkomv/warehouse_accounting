package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Employee;
import com.kata.warehouse_accounting.model.dto.EmployeeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface EmployeeMapper {
    EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);

    EmployeeDTO employeeToDTO(Employee currency);
    Employee employeeDTOToModel(EmployeeDTO employeeDTO);

}
