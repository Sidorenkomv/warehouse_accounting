package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.ChannelSales;
import com.kata.warehouse_accounting.model.dto.ChannelSalesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ChannelSalesMapper {
    ChannelSalesMapper CHANNEL_SALES_MAPPER = Mappers.getMapper(ChannelSalesMapper.class);

    @Mapping(source = "ownerEmployee", target = "ownerEmployee.id")
    @Mapping(source = "whoChangedIt", target = "whoChangedIt.id")
    ChannelSales toEntity(ChannelSalesDTO channelSalesDTO);

    @Mapping(source = "ownerEmployee.id", target = "ownerEmployee")
    @Mapping(source = "whoChangedIt.id", target = "whoChangedIt")
    ChannelSalesDTO toResponse(ChannelSales channelSales);
}
