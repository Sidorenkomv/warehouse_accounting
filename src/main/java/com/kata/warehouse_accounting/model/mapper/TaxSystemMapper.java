package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.TaxSystem;
import com.kata.warehouse_accounting.model.dto.TaxSystemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TaxSystemMapper {

    TaxSystemMapper INSTANCE = Mappers.getMapper(TaxSystemMapper.class);

    TaxSystemDTO taxSystemToDto(TaxSystem taxSystem);

    TaxSystem taxSystemToModel(TaxSystemDTO taxSystemDTO);
}
