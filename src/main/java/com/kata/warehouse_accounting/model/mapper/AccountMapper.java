package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Account;
import com.kata.warehouse_accounting.model.dto.AccountDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AccountMapper {
    AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    AccountDTO accountToDTO(Account Account);
    Account accountDTOToModel(AccountDTO AccountDTO);

}
