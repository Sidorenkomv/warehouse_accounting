package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.BarCode;
import com.kata.warehouse_accounting.model.ValueAddedTax;
import com.kata.warehouse_accounting.model.dto.BarCodeDTO;
import com.kata.warehouse_accounting.model.dto.ValueAddedTaxDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BarCodeMapper {
    BarCodeMapper INSTANCE = Mappers.getMapper(BarCodeMapper.class);

    BarCodeDTO toDto (BarCode barCode);
    BarCode toModel(BarCodeDTO barCodeDTO);
}
