package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.FileObject;
import com.kata.warehouse_accounting.model.dto.FileObjectDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface FileObjectMapper {

    FileObjectMapper INSTANCE = Mappers.getMapper(FileObjectMapper.class);

    FileObjectDTO fileObjectToDto(FileObject fileObject);

    FileObject fileObjectToModel(FileObjectDTO fileObjectDTO);
}
