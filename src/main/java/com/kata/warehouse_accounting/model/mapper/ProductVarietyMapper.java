package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.ProductVariety;
import com.kata.warehouse_accounting.model.dto.ProductVarietyDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ProductVarietyMapper {
    ProductVarietyMapper INSTANCE = Mappers.getMapper(ProductVarietyMapper.class);

    ProductVarietyDTO toDto(ProductVariety productVariety);

    ProductVariety toModel(ProductVarietyDTO productVarietyDTO);
}
