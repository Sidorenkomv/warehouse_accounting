package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Document;
import com.kata.warehouse_accounting.model.dto.DocumentDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DocumentMapper {
    DocumentMapper INSTANCE = Mappers.getMapper(DocumentMapper.class);

    DocumentDTO documentToDTO(Document document);
    Document documentDTOToModel(DocumentDTO documentDTO);
}
