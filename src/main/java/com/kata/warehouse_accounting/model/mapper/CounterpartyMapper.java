package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Counterparty;
import com.kata.warehouse_accounting.model.dto.CounterpartyRequestDTO;
import com.kata.warehouse_accounting.model.dto.CounterpartyResponseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CounterpartyMapper {
    CounterpartyMapper COUNTERPARTY_MAPPER = Mappers.getMapper(CounterpartyMapper.class);

    Counterparty toEntity(CounterpartyRequestDTO counterpartyRequestDTO);

    CounterpartyResponseDTO toResponse(Counterparty counterparty);
}
