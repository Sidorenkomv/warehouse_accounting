package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.PaymentAccount;
import com.kata.warehouse_accounting.model.dto.PaymentAccountDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface PaymentAccountMapper {
    PaymentAccountMapper PAYMENT_ACCOUNT = Mappers.getMapper(PaymentAccountMapper.class);

    PaymentAccount toEntity(PaymentAccountDTO paymentAccountDTO);

    PaymentAccountDTO toResponse(PaymentAccount paymentAccount);
}
