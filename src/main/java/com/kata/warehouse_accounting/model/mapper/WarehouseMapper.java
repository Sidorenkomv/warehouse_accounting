package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Warehouse;
import com.kata.warehouse_accounting.model.dto.WarehouseDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface WarehouseMapper {
    WarehouseMapper INSTANCE = Mappers.getMapper(WarehouseMapper.class);

    WarehouseDTO warehouseToDTO(Warehouse warehouse);
    Warehouse warehouseDTOToModel(WarehouseDTO warehouseDTO);
}
