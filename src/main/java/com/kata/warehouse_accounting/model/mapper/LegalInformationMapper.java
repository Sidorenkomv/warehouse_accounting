package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.LegalInformation;
import com.kata.warehouse_accounting.model.dto.LegalInformationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface LegalInformationMapper {

    LegalInformationMapper INSTANCE = Mappers.getMapper(LegalInformationMapper.class);

    LegalInformationDTO legalInformationToDto(LegalInformation legalInformation);

    LegalInformation legalInformationToModel(LegalInformationDTO legalInformationDTO);
}
