package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.GoodsGroup;
import com.kata.warehouse_accounting.model.dto.GoodsGroupDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GoodsGroupMapper {

    GoodsGroupMapper GROUP_MAPPER = Mappers.getMapper(GoodsGroupMapper.class);

    GoodsGroup GoodsGroupToModel(GoodsGroupDTO goodsGroupDTO);

    GoodsGroupDTO GoodsGroupToDTO(GoodsGroup goodsGroup);

}
