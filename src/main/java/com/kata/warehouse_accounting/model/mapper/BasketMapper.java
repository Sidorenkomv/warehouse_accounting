package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.Basket;
import com.kata.warehouse_accounting.model.dto.BasketDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface BasketMapper {
    BasketMapper INSTANCE = Mappers.getMapper(BasketMapper.class);

    BasketDTO basketToDTO(Basket basket);
    Basket basketDTOToModel(BasketDTO basketDTO);
}
