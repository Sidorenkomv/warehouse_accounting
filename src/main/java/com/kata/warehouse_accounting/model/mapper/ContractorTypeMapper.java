package com.kata.warehouse_accounting.model.mapper;

import com.kata.warehouse_accounting.model.ContractorType;
import com.kata.warehouse_accounting.model.dto.ContractorTypeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ContractorTypeMapper {

    ContractorTypeMapper INSTANCE = Mappers.getMapper(ContractorTypeMapper.class);

    ContractorTypeDTO contractorTypeToDto(ContractorType contractorType);

    ContractorType contractorTypeToModel(ContractorTypeDTO legalInformationDTO);
}
