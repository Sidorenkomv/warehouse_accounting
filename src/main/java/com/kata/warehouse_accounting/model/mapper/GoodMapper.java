package com.kata.warehouse_accounting.model.mapper;


import com.kata.warehouse_accounting.model.Good;
import com.kata.warehouse_accounting.model.dto.GoodDTO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface GoodMapper {

    GoodMapper GOOD_MAPPER = Mappers.getMapper(GoodMapper.class);

    Good GoodToModel(GoodDTO goodDTO);

    GoodDTO GoodToDTO(Good good);
}
