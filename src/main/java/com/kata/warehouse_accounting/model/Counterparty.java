package com.kata.warehouse_accounting.model;

import com.kata.warehouse_accounting.model.support.PricesType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "counterparty")
@SQLDelete(sql = "UPDATE counterparty SET deleted = true WHERE id=?")
@Where(clause = "deleted = false")
public class Counterparty {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "status_id")
    private Status status;

    @ManyToMany(fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @JoinTable(name = "counterparty_groups",
            joinColumns = @JoinColumn(name = "counterparty_id"),
            inverseJoinColumns = @JoinColumn(name = "groupsCounterparty_id"))
    private Set<GroupsCounterparty> groupsCounterparty;

    @Column(name = "phone")
    private Long phone;

    @Column(name = "fax")
    private Long fax;

    @Column(name = "email")
    private String email;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "address_id")
    private Address address;

    @Column(name = "commentAddress")
    private String commentAddress;

    @Column(name = "comment")
    private String comment;

    @Column(name = "cod")
    private String cod;


    @Column(name = "externalCode")
    private String externalCode = RandomStringUtils.random(22, "abcdefghijklmnopqrstuvwxyz" +
            "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "-");

    @OneToMany(cascade = CascadeType.ALL)
    private List<ContactPerson> contactPersons;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "legalDetails_id")
    private LegalDetails legalDetails;

    @OneToMany(cascade = CascadeType.ALL)
    private List<PaymentAccount> paymentAccount;

    @Column(name = "prices")
    private PricesType prices;

    @Column(name = "discountNumber")
    private String discountNumber;

    @ManyToOne(cascade = CascadeType.ALL)
    private Employee ownerEmployee;

    @Column(name = "ownerDepartment")
    private String ownerDepartment;

    @Column(name = "dateOfChange")
    private LocalDateTime dateOfChange;

    @ManyToOne(cascade = CascadeType.ALL)
    private Employee whoChangedIt;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;
}
