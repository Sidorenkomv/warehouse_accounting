package com.kata.warehouse_accounting.model;

import com.kata.warehouse_accounting.model.support.ProductType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_variety")
@SQLDelete(sql = "UPDATE product_variety SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class ProductVariety {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "productType")
    private ProductType type;

    @Column(name = "deleted")
    private boolean isDeleted = Boolean.FALSE;

}
