package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "product_details")
@SQLDelete(sql = "UPDATE product_details SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class ProductDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "c_n_f_e_a")
    private String commodityNomenclatureOfForeignEconomicActivity;

    @Column(name = "target_gender")
    private String targetGender;

    @Column(name = "type_of_production")
    private String typeOfProduction;

    @Column(name = "age_category")
    private String ageCategory;

    @Column(name = "model")
    private String model;

    @Column(name = "is_Set")
    private boolean isSet = Boolean.FALSE;

    @Column(name = "is_traceable")
    private boolean isTraceable = Boolean.FALSE;

    @Column(name = "is_partial_sale")
    private boolean isPartialSale = Boolean.FALSE;

    @Column(name = "deleted")
    private boolean isDeleted = Boolean.FALSE;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "product_variety_id", referencedColumnName = "id")
    private ProductVariety productVarietyId;

}
