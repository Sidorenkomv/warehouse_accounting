package com.kata.warehouse_accounting.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "value_added_tax")
@SQLDelete(sql = "UPDATE value_added_tax SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class ValueAddedTax {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type")
    private String type;

    @Column(name = "vatRate")
    private byte vatRate;

    @Column(name = "comment")
    private String comment;

    @Column(name = "dateOfChange")
    private LocalDateTime dateOfChange;

    @Column(name = "deleted")
    private boolean deleted;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "employee_id")
    private Employee ownerEmployee;

    @Column(name = "owner_department")
    private String ownerDepartment;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "whoChangedIt")
    private Employee whoChangedIt;
}
