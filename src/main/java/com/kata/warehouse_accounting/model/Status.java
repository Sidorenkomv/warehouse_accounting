package com.kata.warehouse_accounting.model;

import com.kata.warehouse_accounting.model.support.StatusColor;
import com.kata.warehouse_accounting.model.support.StatusType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "status")
@SQLDelete(sql = "UPDATE status SET deleted = true WHERE id=?")
@Where(clause = "deleted = false")
public class Status {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "statusType")
    private StatusType statusType;

    @Column(name = "statusColor")
    private StatusColor statusColor;

    @Column(name = "deleted")
    private boolean deleted = Boolean.FALSE;
}
