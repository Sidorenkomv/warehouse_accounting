package com.kata.warehouse_accounting.model;

import com.kata.warehouse_accounting.model.support.CodeType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "bar_code")
@SQLDelete(sql = "UPDATE bar_code SET deleted = true WHERE id=?")
@Where(clause = "deleted=false")
public class BarCode {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "codeType")
    private CodeType codeType;

    @Column(name = "codeDigital")
    private String codeDigital;

    @Column(name = "deleted")
    private boolean deleted;

}
