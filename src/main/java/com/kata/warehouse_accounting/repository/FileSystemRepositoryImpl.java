package com.kata.warehouse_accounting.repository;

import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Objects;

@Repository
public class FileSystemRepositoryImpl implements FileSystemRepository {
    String RESOURCES_DIR = Objects.requireNonNull(FileObjectRepository
            .class.getResource("/")).getPath();

    @Override
    public String save(byte[] content, String fileName) throws IOException {
        Path newFile = Paths.get(RESOURCES_DIR +
                new Date().getTime() + "-" + fileName);
        Files.createDirectories(newFile.getParent());
        Files.write(newFile, content);
        return newFile.toAbsolutePath()
                .toString();
    }

    @Override
    public FileSystemResource findInFileSystem(String location) {
        try {
            return new FileSystemResource(Paths.get(location));
        } catch (Exception e) {
            throw new RuntimeException();
        }
    }
}
