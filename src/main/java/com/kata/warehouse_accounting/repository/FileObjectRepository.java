package com.kata.warehouse_accounting.repository;

import com.kata.warehouse_accounting.model.FileObject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FileObjectRepository extends JpaRepository<FileObject, Long> {
}
