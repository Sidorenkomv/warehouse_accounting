package com.kata.warehouse_accounting.repository;

import com.kata.warehouse_accounting.model.TaxSystem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TaxSystemRepository extends JpaRepository<TaxSystem, Long> {
}
