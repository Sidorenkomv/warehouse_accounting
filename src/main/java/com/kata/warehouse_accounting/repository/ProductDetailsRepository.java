package com.kata.warehouse_accounting.repository;

import com.kata.warehouse_accounting.model.ProductDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDetailsRepository extends JpaRepository<ProductDetails, Long> {
}
