package com.kata.warehouse_accounting.repository;

import com.kata.warehouse_accounting.model.LegalDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LegalDetailsRepository extends JpaRepository<LegalDetails, Long> {
}
