package com.kata.warehouse_accounting.repository;

import com.kata.warehouse_accounting.model.BarCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BarCodeRepository extends JpaRepository<BarCode, Long> {

}
