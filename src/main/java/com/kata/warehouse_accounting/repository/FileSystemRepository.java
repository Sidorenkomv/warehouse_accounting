package com.kata.warehouse_accounting.repository;

import org.springframework.core.io.FileSystemResource;

import java.io.IOException;

public interface FileSystemRepository {

    String save(byte[] content, String fileName) throws IOException;

    /*
     * Use InputStreamResource or ByteArrayResource
     * when using cloud storage
     * */
    FileSystemResource findInFileSystem(String location);
}
