package com.kata.warehouse_accounting.repository;

import com.kata.warehouse_accounting.model.GroupsCounterparty;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GroupsCounterpartyRepository extends JpaRepository<GroupsCounterparty, Long> {

}
