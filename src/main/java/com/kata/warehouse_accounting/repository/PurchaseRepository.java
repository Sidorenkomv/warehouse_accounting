package com.kata.warehouse_accounting.repository;

import com.kata.warehouse_accounting.model.Purchase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PurchaseRepository extends JpaRepository<Purchase, Long> {
}
