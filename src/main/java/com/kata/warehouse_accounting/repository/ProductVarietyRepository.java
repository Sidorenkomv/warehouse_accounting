package com.kata.warehouse_accounting.repository;

import com.kata.warehouse_accounting.model.ProductVariety;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductVarietyRepository extends JpaRepository<ProductVariety, Long> {

}
