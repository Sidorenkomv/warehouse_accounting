package com.kata.warehouse_accounting.repository;

import com.kata.warehouse_accounting.model.ContractorType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContractorTypeRepository extends JpaRepository<ContractorType, Long>  {
}
