package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.PaymentAccountDTO;
import com.kata.warehouse_accounting.model.mapper.PaymentAccountMapper;
import com.kata.warehouse_accounting.repository.PaymentAccountRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PaymentAccountServiceImpl implements PaymentAccountService {

    private final PaymentAccountRepository paymentAccountRepository;

    public PaymentAccountServiceImpl(PaymentAccountRepository paymentAccountRepository) {
        this.paymentAccountRepository = paymentAccountRepository;
    }

    @Override
    public List<PaymentAccountDTO> findAll() {
        return paymentAccountRepository.findAll()
                .stream()
                .map(PaymentAccountMapper.PAYMENT_ACCOUNT::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    public PaymentAccountDTO findById(Long id) {
        return PaymentAccountMapper
                .PAYMENT_ACCOUNT
                .toResponse(paymentAccountRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(PaymentAccountDTO paymentAccountDTO) {
        paymentAccountRepository.save(PaymentAccountMapper
                .PAYMENT_ACCOUNT
                .toEntity(paymentAccountDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (paymentAccountRepository.existsById(id)) {
            paymentAccountRepository.deleteById(id);
        }
    }
}
