package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.CurrencyDTO;

import java.util.List;

public interface CurrencyService {
    List<CurrencyDTO> getAll();

    CurrencyDTO getById(Long id);

    void create(CurrencyDTO currencyDTO);

    void update(CurrencyDTO currencyDTO);

    void deleteById(Long id);


}
