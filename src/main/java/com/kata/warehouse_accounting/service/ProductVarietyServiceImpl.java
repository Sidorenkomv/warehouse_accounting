package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ProductVarietyDTO;
import com.kata.warehouse_accounting.model.mapper.ProductVarietyMapper;
import com.kata.warehouse_accounting.repository.ProductVarietyRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductVarietyServiceImpl implements ProductVarietyService {

    private final ProductVarietyRepository productVarietyRepository;

    public ProductVarietyServiceImpl(ProductVarietyRepository productVarietyRepository) {
        this.productVarietyRepository = productVarietyRepository;
    }

    @Override
    public List<ProductVarietyDTO> findAll() {
        List<ProductVarietyDTO> productVarietyDTOList = productVarietyRepository.findAll().stream()
                .map(ProductVarietyMapper.INSTANCE::toDto)
                .collect(Collectors.toList());
        return productVarietyDTOList;
    }

    @Override
    public ProductVarietyDTO findById(Long id) {
        return ProductVarietyMapper.INSTANCE.toDto(productVarietyRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(ProductVarietyDTO productVarietyDTO) {
        productVarietyRepository.save(ProductVarietyMapper.INSTANCE.toModel(productVarietyDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (productVarietyRepository.existsById(id)) {
            productVarietyRepository.deleteById(id);
        }
    }
}
