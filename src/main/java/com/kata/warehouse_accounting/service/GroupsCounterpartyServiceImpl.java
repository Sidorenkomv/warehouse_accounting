package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.GroupsCounterpartyDTO;
import com.kata.warehouse_accounting.model.mapper.GroupsCounterpartyMapper;
import com.kata.warehouse_accounting.repository.GroupsCounterpartyRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class GroupsCounterpartyServiceImpl implements GroupsCounterpartyService {

    private final GroupsCounterpartyRepository groupsCounterpartyRepository;

    public GroupsCounterpartyServiceImpl(GroupsCounterpartyRepository groupsCounterpartyRepository) {
        this.groupsCounterpartyRepository = groupsCounterpartyRepository;
    }

    @Override
    public List<GroupsCounterpartyDTO> findAll() {
        return groupsCounterpartyRepository.findAll()
                .stream()
                .map(GroupsCounterpartyMapper.GROUPS_COUNTERPARTY_MAPPER::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    public GroupsCounterpartyDTO findById(Long id) {
        return GroupsCounterpartyMapper
                .GROUPS_COUNTERPARTY_MAPPER
                .toResponse(groupsCounterpartyRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(GroupsCounterpartyDTO groupsCounterpartyDTO) {
        groupsCounterpartyRepository.save(GroupsCounterpartyMapper
                .GROUPS_COUNTERPARTY_MAPPER
                .toEntity(groupsCounterpartyDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (groupsCounterpartyRepository.existsById(id)) {
            groupsCounterpartyRepository.deleteById(id);
        }
    }
}
