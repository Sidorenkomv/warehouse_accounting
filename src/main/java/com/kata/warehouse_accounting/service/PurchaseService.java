package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.Purchase;
import com.kata.warehouse_accounting.model.dto.PurchaseDto;
import org.springframework.stereotype.Service;

import java.util.List;


public interface PurchaseService {

    List<PurchaseDto> showAllPurchases();

    PurchaseDto showPurchase(Long id);

    void addPurchase(PurchaseDto purchaseDto);

    void deletePurchase(Long id);

    void editPurchase(PurchaseDto purchaseDto);
}
