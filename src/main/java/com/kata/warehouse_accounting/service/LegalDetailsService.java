package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.LegalDetailsDTO;

import java.util.List;

public interface LegalDetailsService {

    List<LegalDetailsDTO> findAll(boolean isDeleted);

    LegalDetailsDTO findById(Long id);

    void save(LegalDetailsDTO legalDetailDto);

    void deleteById(Long id);
}
