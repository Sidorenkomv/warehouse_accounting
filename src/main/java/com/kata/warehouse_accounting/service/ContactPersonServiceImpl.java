package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ContactPersonDTO;
import com.kata.warehouse_accounting.model.mapper.ContactPersonMapper;
import com.kata.warehouse_accounting.repository.ContactPersonRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContactPersonServiceImpl implements ContactPersonService {
    private final ContactPersonRepository contactPersonRepository;

    public ContactPersonServiceImpl(ContactPersonRepository contactPersonRepository) {
        this.contactPersonRepository = contactPersonRepository;
    }

    @Override
    public List<ContactPersonDTO> findAll() {
        return contactPersonRepository.findAll()
                .stream()
                .map(ContactPersonMapper.CONTACT_PERSON_MAPPER::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    public ContactPersonDTO findById(Long id) {
        return ContactPersonMapper
                .CONTACT_PERSON_MAPPER
                .toResponse(contactPersonRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(ContactPersonDTO contactPersonDTO) {
        contactPersonRepository.save(ContactPersonMapper
                .CONTACT_PERSON_MAPPER
                .toEntity(contactPersonDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (contactPersonRepository.existsById(id)) {
            contactPersonRepository.deleteById(id);
        }
    }
}
