package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.BasketDTO;

import java.util.List;

public interface BasketService {
    List<BasketDTO> getAll();

    BasketDTO getById(Long id);

    void create(BasketDTO basketDTO);

    void update(BasketDTO basketDTO);

    void deleteById(Long id);
}
