package com.kata.warehouse_accounting.service;


import com.kata.warehouse_accounting.model.Purchase;
import com.kata.warehouse_accounting.model.dto.PurchaseDto;
import com.kata.warehouse_accounting.model.mapper.PurchaseMapper;
import com.kata.warehouse_accounting.repository.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PurchaseServiceImpl implements PurchaseService {

    PurchaseRepository purchaseRepository;

    @Autowired
    public PurchaseServiceImpl(PurchaseRepository purchaseRepository) {
        this.purchaseRepository = purchaseRepository;
    }

    @Override
    public List<PurchaseDto> showAllPurchases() {
        List<PurchaseDto> purchaseDtos = new ArrayList<>();
        List<Purchase> purchases = purchaseRepository.findAll();
        for (Purchase purchase : purchases) {
            purchaseDtos.add(PurchaseMapper.INSTANCE.purchaseToDto(purchase));
        }
        return purchaseDtos;
    }

    @Override
    public PurchaseDto showPurchase(Long id) {
        Optional<Purchase> optionalPurchase = purchaseRepository.findById(id);
        if (!optionalPurchase.isPresent()) {
            return null;
        }
        Purchase purchase = optionalPurchase.get();
        return PurchaseMapper.INSTANCE.purchaseToDto(purchase);
    }

    @Override
    @Transactional
    public void addPurchase(PurchaseDto purchaseDto) {
        purchaseRepository.save(PurchaseMapper.INSTANCE.purchaseDtoToModel(purchaseDto));
    }

    @Override
    @Transactional
    public void deletePurchase(Long id) {
        purchaseRepository.deleteById(id);

    }

    @Override
    @Transactional
    public void editPurchase(PurchaseDto purchaseDto) {

    }
}
