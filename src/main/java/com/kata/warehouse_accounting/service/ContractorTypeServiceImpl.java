package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ContractorTypeDTO;
import com.kata.warehouse_accounting.model.mapper.ContractorTypeMapper;
import com.kata.warehouse_accounting.repository.ContractorTypeRepository;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ContractorTypeServiceImpl implements ContractorTypeService {

    private final ContractorTypeRepository contractorTypeRepository;
    private final EntityManager entityManager;

    @Autowired
    public ContractorTypeServiceImpl(ContractorTypeRepository
                                                 contractorTypeRepository,
                                     EntityManager entityManager) {
        this.contractorTypeRepository = contractorTypeRepository;
        this.entityManager = entityManager;
    }

    @Override
    public List<ContractorTypeDTO> findAll(boolean isDeleted) {
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedContractorType");
        filter.setParameter("isDeleted", isDeleted);
        List<ContractorTypeDTO> contractorTypeList = contractorTypeRepository
                .findAll()
                .stream()
                .map(ContractorTypeMapper.INSTANCE::contractorTypeToDto)
                .collect(Collectors.toList());
        session.disableFilter("deletedContractorType");
        return contractorTypeList;
    }

    @Override
    public ContractorTypeDTO findById(Long id) {
        return ContractorTypeMapper
                .INSTANCE
                .contractorTypeToDto(contractorTypeRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(ContractorTypeDTO contractorTypeDTO) {
        contractorTypeRepository.save(ContractorTypeMapper
                .INSTANCE
                .contractorTypeToModel(contractorTypeDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (contractorTypeRepository.existsById(id)) {
            contractorTypeRepository.deleteById(id);
        }
    }
}
