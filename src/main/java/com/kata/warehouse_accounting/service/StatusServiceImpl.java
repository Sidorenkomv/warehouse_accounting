package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.StatusDTO;
import com.kata.warehouse_accounting.model.mapper.StatusMapper;
import com.kata.warehouse_accounting.repository.StatusRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class StatusServiceImpl implements StatusService {

    private final StatusRepository statusRepository;

    public StatusServiceImpl(StatusRepository statusRepository) {
        this.statusRepository = statusRepository;
    }

    @Override
    public List<StatusDTO> findAll() {
        return statusRepository.findAll()
                .stream()
                .map(StatusMapper.STATUS_MAPPER::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    @Transactional
    public void save(StatusDTO statusDTO) {
        statusRepository.save(StatusMapper
                .STATUS_MAPPER
                .toEntity(statusDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (statusRepository.existsById(id)) {
            statusRepository.deleteById(id);
        }
    }
}
