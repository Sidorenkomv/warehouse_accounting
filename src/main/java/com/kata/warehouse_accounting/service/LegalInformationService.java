package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.LegalInformationDTO;

import java.util.List;

public interface LegalInformationService {

    List<LegalInformationDTO> findAll(boolean isDeleted);

    LegalInformationDTO findById(Long id);

    void save(LegalInformationDTO legalInformationDTO);

    void deleteById(Long id);
}
