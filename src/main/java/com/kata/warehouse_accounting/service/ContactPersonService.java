package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ContactPersonDTO;

import java.util.List;

public interface ContactPersonService {
    List<ContactPersonDTO> findAll();

    ContactPersonDTO findById(Long id);

    void save(ContactPersonDTO contactPersonDTO);

    void deleteById(Long id);
}
