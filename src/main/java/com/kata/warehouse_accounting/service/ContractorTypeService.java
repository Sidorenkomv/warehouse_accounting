package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ContractorTypeDTO;
import com.kata.warehouse_accounting.model.dto.LegalDetailsDTO;

import java.util.List;

public interface ContractorTypeService {

    List<ContractorTypeDTO> findAll(boolean isDeleted);

    ContractorTypeDTO findById(Long id);

    void save(ContractorTypeDTO contractorTypeDTO);

    void deleteById(Long id);
}
