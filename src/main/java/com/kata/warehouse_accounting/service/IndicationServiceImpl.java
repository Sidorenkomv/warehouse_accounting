package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.AccountDTO;
import com.kata.warehouse_accounting.model.dto.IndicationDTO;
import com.kata.warehouse_accounting.model.mapper.AccountMapper;
import com.kata.warehouse_accounting.model.mapper.IndicationMapper;
import com.kata.warehouse_accounting.repository.AccountRepository;
import com.kata.warehouse_accounting.repository.IndicationRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class IndicationServiceImpl implements IndicationService{

    private final IndicationRepository indicationRepository;

    public IndicationServiceImpl(IndicationRepository indicationRepository) {
        this.indicationRepository = indicationRepository;
    }

    @Override
    public List<IndicationDTO> getAll() {
        return indicationRepository.findAll()
                .stream()
                .map(IndicationMapper.INSTANCE::indicationToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public IndicationDTO getById(Long id) {
        return IndicationMapper
                .INSTANCE
                .indicationToDTO(indicationRepository.findById(id).orElse(null));
    }

    @Override
    public void create(IndicationDTO indicationDTO) {
        indicationRepository.save(IndicationMapper
                .INSTANCE
                .indicationDTOToModel(indicationDTO));
    }

    @Override
    public void update(IndicationDTO indicationDTO) {
        indicationRepository.save(IndicationMapper
                .INSTANCE
                .indicationDTOToModel(indicationDTO));
    }

    @Override
    public void deleteById(Long id) {
        indicationRepository.deleteById(id);
    }
}
