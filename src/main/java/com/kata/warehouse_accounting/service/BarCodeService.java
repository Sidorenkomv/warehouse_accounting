package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.BarCodeDTO;
import java.util.List;

public interface BarCodeService {
    List<BarCodeDTO> findAll();

    BarCodeDTO findById(Long id);

    void save(BarCodeDTO barCodeDTO);

    void deleteById(Long id);
}
