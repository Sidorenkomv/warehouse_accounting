package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.FileObject;
import com.kata.warehouse_accounting.repository.FileObjectRepository;
import com.kata.warehouse_accounting.repository.FileSystemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.nio.file.FileAlreadyExistsException;

@Service
public class FileStorageServiceImpl implements FileStorageService {
    private final FileObjectRepository fileObjectRepository;
    private final FileSystemRepository fileSystemRepository;

    @Autowired
    public FileStorageServiceImpl(FileObjectRepository fileObjectRepository,
                                  FileSystemRepository fileSystemRepository) {
        this.fileObjectRepository = fileObjectRepository;
        this.fileSystemRepository = fileSystemRepository;
    }

    @Override
    public Long upload(MultipartFile file) {
        try {
            FileObject fileObject = new FileObject();
            fileObject.setContent(file.getBytes());
            fileObject.setName(file.getName());
            return fileObject.getId();
        } catch (Exception e) {
            if (e instanceof FileAlreadyExistsException) {
                throw new RuntimeException("A file of that name already exists.");
            }
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public Resource download(Long id) {
        byte[] file = fileObjectRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND))
                .getContent();
        return new ByteArrayResource(file);
    }

    @Override
    public Long save(byte[] bytes, String fileName) {
        try {
            String url = fileSystemRepository.save(bytes, fileName);
            return fileObjectRepository.save(new FileObject(fileName, url)).getId();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public Long save(MultipartFile file) {
        try {
            String fileName = file.getOriginalFilename();
            String url = fileSystemRepository.save(file.getBytes(), fileName);
            return fileObjectRepository.save(new FileObject(fileName, url)).getId();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public FileSystemResource find(Long id) {
        FileObject fileObject = fileObjectRepository.findById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        return fileSystemRepository.findInFileSystem(fileObject.getUrl());
    }
}
