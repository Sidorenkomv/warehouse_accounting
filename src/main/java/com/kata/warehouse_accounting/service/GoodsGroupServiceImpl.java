package com.kata.warehouse_accounting.service;


import com.kata.warehouse_accounting.model.GoodsGroup;
import com.kata.warehouse_accounting.model.dto.GoodsGroupDTO;
import com.kata.warehouse_accounting.model.mapper.GoodsGroupMapper;
import com.kata.warehouse_accounting.repository.GoodsGroupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GoodsGroupServiceImpl implements GoodsGroupService {

    GoodsGroupRepository goodsGroupRepository;

    @Autowired
    public void setProductGroupRepository(GoodsGroupRepository goodsGroupRepository) {
        this.goodsGroupRepository = goodsGroupRepository;
    }

    public List<GoodsGroupDTO> showAllGroups() {
        List<GoodsGroupDTO> groupDTOS = new ArrayList<>();
        List<GoodsGroup> goodsGroups = goodsGroupRepository.findAll();
        for (GoodsGroup group : goodsGroups) {
            groupDTOS.add(GoodsGroupMapper.GROUP_MAPPER.GoodsGroupToDTO(group));
        }
        return groupDTOS;
    }

    public GoodsGroupDTO showGroupById(Long id) {
        Optional<GoodsGroup> goodsGroupOptional = goodsGroupRepository.findById(id);
        if (!goodsGroupOptional.isPresent()) {
            return null;
        }
        GoodsGroup goodsGroup = goodsGroupOptional.get();
        return GoodsGroupMapper.GROUP_MAPPER.GoodsGroupToDTO(goodsGroup);

    }

    @Transactional
    public void addGroup(GoodsGroupDTO goodsGroupDTO) {
        goodsGroupRepository.save(GoodsGroupMapper.GROUP_MAPPER.GoodsGroupToModel(goodsGroupDTO));
    }

    @Transactional
    public void deleteGroup(Long id) {
        goodsGroupRepository.deleteById(id);
    }

    @Transactional
    public void editGroup(GoodsGroupDTO goodsGroup) {
            goodsGroupRepository.save(GoodsGroupMapper.GROUP_MAPPER.GoodsGroupToModel(goodsGroup));
    }


}
