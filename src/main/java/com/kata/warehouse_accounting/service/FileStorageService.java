package com.kata.warehouse_accounting.service;

import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface FileStorageService {

    Long upload(MultipartFile file);

    Resource download(Long id);

    Long save(byte[] bytes, String fileName);

    Long save(MultipartFile file);

    FileSystemResource find(Long id);

}
