package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.DocumentDTO;

import java.util.List;

public interface DocumentService {
    List<DocumentDTO> getAll();

    DocumentDTO getById(Long id);

    void create(DocumentDTO documentDTO);

    void update(DocumentDTO documentDTO);

    void deleteById(Long id);
}
