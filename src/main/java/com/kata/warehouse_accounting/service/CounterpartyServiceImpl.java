package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.CounterpartyRequestDTO;
import com.kata.warehouse_accounting.model.dto.CounterpartyResponseDTO;
import com.kata.warehouse_accounting.model.mapper.CounterpartyMapper;
import com.kata.warehouse_accounting.repository.CounterpartyRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CounterpartyServiceImpl implements CounterpartyService {
    private final CounterpartyRepository counterpartyRepository;

    public CounterpartyServiceImpl(CounterpartyRepository counterpartyRepository) {
        this.counterpartyRepository = counterpartyRepository;
    }

    @Override
    public List<CounterpartyResponseDTO> findAll() {
        return counterpartyRepository.findAll()
                .stream()
                .map(CounterpartyMapper.COUNTERPARTY_MAPPER::toResponse)
                .collect(Collectors.toList());
    }

    @Override
    public CounterpartyResponseDTO findById(Long id) {
        return CounterpartyMapper
                .COUNTERPARTY_MAPPER
                .toResponse(counterpartyRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(CounterpartyRequestDTO counterpartyRequestDTO) {
        counterpartyRepository.save(CounterpartyMapper
                .COUNTERPARTY_MAPPER
                .toEntity(counterpartyRequestDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (counterpartyRepository.existsById(id)) {
            counterpartyRepository.deleteById(id);
        }
    }
}
