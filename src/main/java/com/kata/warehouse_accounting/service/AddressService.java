package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.AddressDTO;

import java.util.List;

public interface AddressService {
    List<AddressDTO> getAll();

    AddressDTO getById(Long id);

    void create(AddressDTO addressDTO);

    void update(AddressDTO addressDTO);

    void deleteById(Long id);

}
