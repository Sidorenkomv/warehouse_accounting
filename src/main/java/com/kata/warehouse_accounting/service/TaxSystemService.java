package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.LegalDetailsDTO;
import com.kata.warehouse_accounting.model.dto.TaxSystemDTO;

import java.util.List;

public interface TaxSystemService {

    List<TaxSystemDTO> findAll(boolean isDeleted);

    TaxSystemDTO findById(Long id);

    void save(TaxSystemDTO taxSystemDTO);

    void deleteById(Long id);
}
