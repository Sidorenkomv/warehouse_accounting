package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.AccountDTO;
import com.kata.warehouse_accounting.model.mapper.AccountMapper;
import com.kata.warehouse_accounting.repository.AccountRepository;
import com.kata.warehouse_accounting.service.AccountService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AccountServiceImpl implements AccountService {
    private final AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Override
    public List<AccountDTO> getAll() {
        return accountRepository.findAll()
                .stream()
                .map(AccountMapper.INSTANCE::accountToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public AccountDTO getById(Long id) {
        return AccountMapper
                .INSTANCE
                .accountToDTO(accountRepository.findById(id).orElse(null));
    }

    @Override
    public void create(AccountDTO accountDTO) {
        accountRepository.save(AccountMapper
                .INSTANCE
                .accountDTOToModel(accountDTO));
    }

    @Override
    public void update(AccountDTO accountDTO) {
        accountRepository.save(AccountMapper
                .INSTANCE
                .accountDTOToModel(accountDTO));
    }

    @Override
    public void deleteById(Long id) {
        accountRepository.deleteById(id);
    }
}
