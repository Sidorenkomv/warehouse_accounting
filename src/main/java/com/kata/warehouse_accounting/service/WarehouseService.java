package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.WarehouseDTO;

import java.util.List;

public interface WarehouseService {
    List<WarehouseDTO> getAll();

    WarehouseDTO getById(Long id);

    void create(WarehouseDTO warehouseDTO);

    void update(WarehouseDTO warehouseDTO);

    void deleteById(Long id);

}
