package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ChannelSalesDTO;
import java.util.List;

public interface ChannelSalesService {
    List<ChannelSalesDTO> findAll();
    ChannelSalesDTO findById(Long id);

    void save(ChannelSalesDTO channelSalesDTO);

    void deleteById(Long id);
}
