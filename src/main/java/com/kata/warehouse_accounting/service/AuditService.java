package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.AuditDTO;

import java.util.List;

public interface AuditService {
    List<AuditDTO> getAll();

    AuditDTO getById(Long id);

    void create(AuditDTO auditDTO);

    void deleteById(Long id);
}
