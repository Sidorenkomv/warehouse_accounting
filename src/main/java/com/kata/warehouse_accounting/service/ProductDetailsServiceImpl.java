package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ProductDetailsDTO;
import com.kata.warehouse_accounting.model.mapper.ProductDetailsMapper;
import com.kata.warehouse_accounting.repository.ProductDetailsRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductDetailsServiceImpl implements ProductDetailsService {

    private final ProductDetailsRepository productDetailsRepository;

    public ProductDetailsServiceImpl(ProductDetailsRepository productDetailsRepository) {
        this.productDetailsRepository = productDetailsRepository;
    }

    @Override
    public List<ProductDetailsDTO> findAll() {
        List<ProductDetailsDTO> productDetailsDTOList = productDetailsRepository.findAll().stream()
                .map(ProductDetailsMapper.INSTANCE::toDto)
                .collect(Collectors.toList());
        return productDetailsDTOList;
    }

    @Override
    public ProductDetailsDTO findById(Long id) {
        return ProductDetailsMapper.INSTANCE.toDto(productDetailsRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(ProductDetailsDTO productDetailsDTO) {
        productDetailsRepository.save(ProductDetailsMapper.INSTANCE.toModel(productDetailsDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (productDetailsRepository.existsById(id)) {
            productDetailsRepository.deleteById(id);
        }
    }
}
