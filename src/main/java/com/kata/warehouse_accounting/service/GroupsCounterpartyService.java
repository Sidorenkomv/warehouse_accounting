package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.GroupsCounterpartyDTO;

import java.util.List;

public interface GroupsCounterpartyService {
    List<GroupsCounterpartyDTO> findAll();

    GroupsCounterpartyDTO findById(Long id);

    void save(GroupsCounterpartyDTO groupsCounterpartyDTO);

    void deleteById(Long id);
}
