package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.EmployeeDTO;

import java.util.List;

public interface EmployeeService {
    List<EmployeeDTO> getAll();

    EmployeeDTO getById(Long id);

    void create(EmployeeDTO employeeDTO);

    void update(EmployeeDTO employeeDTO);

    void deleteById(Long id);

}
