package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.PaymentAccountDTO;

import java.util.List;

public interface PaymentAccountService {

    List<PaymentAccountDTO> findAll();

    PaymentAccountDTO findById(Long id);

    void save(PaymentAccountDTO paymentAccountDTO);

    void deleteById(Long id);
}
