package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.WarehouseDTO;
import com.kata.warehouse_accounting.model.mapper.WarehouseMapper;
import com.kata.warehouse_accounting.repository.WarehouseRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class WarehouseServiceImpl implements WarehouseService{
    private final WarehouseRepository warehouseRepository;

    public WarehouseServiceImpl(WarehouseRepository warehouseRepository) {
        this.warehouseRepository = warehouseRepository;
    }

    @Override
    public List<WarehouseDTO> getAll() {
        return warehouseRepository.findAll()
                .stream()
                .map(WarehouseMapper.INSTANCE::warehouseToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public WarehouseDTO getById(Long id) {
        return WarehouseMapper
                .INSTANCE
                .warehouseToDTO(warehouseRepository.findById(id).orElse(null));
    }

    @Override
    public void create(WarehouseDTO warehouseDTO) {
        warehouseRepository.save(WarehouseMapper
                .INSTANCE
                .warehouseDTOToModel(warehouseDTO));
    }

    @Override
    public void update(WarehouseDTO warehouseDTO) {
        warehouseRepository.save(WarehouseMapper
                .INSTANCE
                .warehouseDTOToModel(warehouseDTO));
    }

    @Override
    public void deleteById(Long id) {
        warehouseRepository.deleteById(id);
    }

}
