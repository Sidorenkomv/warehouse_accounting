package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.DocumentDTO;
import com.kata.warehouse_accounting.model.mapper.DocumentMapper;
import com.kata.warehouse_accounting.repository.DocumentRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class DocumentServiceImpl implements DocumentService{
    private final DocumentRepository documentRepository;

    public DocumentServiceImpl(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @Override
    public List<DocumentDTO> getAll() {
        return documentRepository.findAll()
                .stream()
                .map(DocumentMapper.INSTANCE::documentToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public DocumentDTO getById(Long id) {
        return DocumentMapper
                .INSTANCE
                .documentToDTO(documentRepository.findById(id).orElse(null));
    }

    @Override
    public void create(DocumentDTO documentDTO) {
        documentRepository.save(DocumentMapper
                .INSTANCE
                .documentDTOToModel(documentDTO));
    }

    @Override
    public void update(DocumentDTO documentDTO) {
        documentRepository.save(DocumentMapper
                .INSTANCE
                .documentDTOToModel(documentDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (documentRepository.existsById(id)) {
            documentRepository.deleteById(id);
        }
    }
}
