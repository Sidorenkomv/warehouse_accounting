package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ChannelSalesDTO;
import com.kata.warehouse_accounting.model.mapper.ChannelSalesMapper;
import com.kata.warehouse_accounting.repository.ChannelSalesRepository;
import org.springframework.stereotype.Service;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ChannelSalesServiceImpl implements ChannelSalesService {

    private final ChannelSalesRepository channelSalesRepository;

    public ChannelSalesServiceImpl(ChannelSalesRepository channelSalesRepository) {
        this.channelSalesRepository = channelSalesRepository;
    }

    @Override
    public List<ChannelSalesDTO> findAll() {
        return channelSalesRepository.findAll()
                .stream()
                .map(ChannelSalesMapper.CHANNEL_SALES_MAPPER::toResponse)
                .collect(Collectors.toList());
    }


    @Override
    public ChannelSalesDTO findById(Long id) {
        return ChannelSalesMapper
                .CHANNEL_SALES_MAPPER
                .toResponse(channelSalesRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(ChannelSalesDTO channelSalesDTO) {
        channelSalesRepository.save(ChannelSalesMapper
                .CHANNEL_SALES_MAPPER
                .toEntity(channelSalesDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (channelSalesRepository.existsById(id)) {
            channelSalesRepository.deleteById(id);
        }
    }
}
