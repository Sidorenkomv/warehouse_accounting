package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.AuditDTO;
import com.kata.warehouse_accounting.model.mapper.AuditMapper;
import com.kata.warehouse_accounting.repository.AuditRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AuditServiceImpl implements AuditService{

    private final AuditRepository auditRepository;

    public AuditServiceImpl(AuditRepository auditRepository) {
        this.auditRepository = auditRepository;
    }

    @Override
    public List<AuditDTO> getAll() {
        return auditRepository.findAll()
                .stream()
                .map(AuditMapper.INSTANCE::auditToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public AuditDTO getById(Long id) {
        return AuditMapper
                .INSTANCE
                .auditToDTO(auditRepository.findById(id).orElse(null));
    }

    @Transactional
    @Override
    public void create(AuditDTO auditDTO) {
        auditRepository.save(AuditMapper
                .INSTANCE
                .auditDTOToModel(auditDTO));
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        if(auditRepository.existsById(id)) {
            auditRepository.deleteById(id);
        }
    }
}