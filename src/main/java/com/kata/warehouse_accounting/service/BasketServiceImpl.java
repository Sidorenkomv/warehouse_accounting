package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.BasketDTO;
import com.kata.warehouse_accounting.model.mapper.BasketMapper;
import com.kata.warehouse_accounting.repository.BasketRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class BasketServiceImpl implements BasketService{
    private final BasketRepository basketRepository;

    public BasketServiceImpl(BasketRepository basketRepository) {
        this.basketRepository = basketRepository;
    }

    @Override
    public List<BasketDTO> getAll() {
        return basketRepository.findAll()
                .stream()
                .map(BasketMapper.INSTANCE::basketToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public BasketDTO getById(Long id) {
        return BasketMapper
                .INSTANCE
                .basketToDTO(basketRepository.findById(id).orElse(null));
    }

    @Override
    public void create(BasketDTO basketDTO) {
        basketRepository.save(BasketMapper
                .INSTANCE
                .basketDTOToModel(basketDTO));
    }

    @Override
    public void update(BasketDTO basketDTO) {
        basketRepository.save(BasketMapper
                .INSTANCE
                .basketDTOToModel(basketDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (basketRepository.existsById(id)) {
            basketRepository.deleteById(id);
        }
    }
}
