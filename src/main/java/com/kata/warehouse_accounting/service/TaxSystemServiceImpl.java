package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.TaxSystemDTO;
import com.kata.warehouse_accounting.model.mapper.TaxSystemMapper;
import com.kata.warehouse_accounting.repository.TaxSystemRepository;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TaxSystemServiceImpl implements TaxSystemService {

    private final TaxSystemRepository taxSystemRepository;
    private final EntityManager entityManager;

    @Autowired
    public TaxSystemServiceImpl(TaxSystemRepository
                                             taxSystemRepository,
                                EntityManager entityManager) {
        this.taxSystemRepository = taxSystemRepository;
        this.entityManager = entityManager;
    }

    @Override
    public List<TaxSystemDTO> findAll(boolean isDeleted) {
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedTaxSystem");
        filter.setParameter("isDeleted", isDeleted);
        List<TaxSystemDTO> taxSystemList = taxSystemRepository
                .findAll()
                .stream()
                .map(TaxSystemMapper.INSTANCE::taxSystemToDto)
                .collect(Collectors.toList());
        session.disableFilter("deletedTaxSystem");
        return taxSystemList;
    }

    @Override
    public TaxSystemDTO findById(Long id) {
        return TaxSystemMapper
                .INSTANCE
                .taxSystemToDto(taxSystemRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(TaxSystemDTO taxSystemDTO) {
        taxSystemRepository.save(TaxSystemMapper
                .INSTANCE
                .taxSystemToModel(taxSystemDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (taxSystemRepository.existsById(id)) {
            taxSystemRepository.deleteById(id);
        }
    }
}
