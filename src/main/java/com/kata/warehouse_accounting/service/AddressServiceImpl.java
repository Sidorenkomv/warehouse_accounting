package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.AddressDTO;
import com.kata.warehouse_accounting.model.mapper.AddressMapper;
import com.kata.warehouse_accounting.repository.AddressRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class AddressServiceImpl implements AddressService{
    private final AddressRepository addressRepository;

    public AddressServiceImpl(AddressRepository addressRepository) {
        this.addressRepository = addressRepository;
    }

    @Override
    public List<AddressDTO> getAll() {
        return addressRepository.findAll()
                .stream()
                .map(AddressMapper.INSTANCE::addressToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public AddressDTO getById(Long id) {
        return AddressMapper
                .INSTANCE
                .addressToDTO(addressRepository.findById(id).orElse(null));
    }

    @Override
    public void create(AddressDTO addressDTO) {
        addressRepository.save(AddressMapper
                .INSTANCE
                .addressDTOToModel(addressDTO));
    }

    @Override
    public void update(AddressDTO addressDTO) {
        addressRepository.save(AddressMapper
                .INSTANCE
                .addressDTOToModel(addressDTO));
    }

    @Override
    public void deleteById(Long id) {
        addressRepository.deleteById(id);
    }

}
