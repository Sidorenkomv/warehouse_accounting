package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.LegalDetailsDTO;
import com.kata.warehouse_accounting.model.dto.LegalInformationDTO;
import com.kata.warehouse_accounting.model.mapper.LegalDetailsMapper;
import com.kata.warehouse_accounting.model.mapper.LegalInformationMapper;
import com.kata.warehouse_accounting.repository.LegalInformationRepository;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LegalInformationServiceImpl implements LegalInformationService {

    private final LegalInformationRepository legalInformationRepository;
    private final EntityManager entityManager;

    @Autowired
    public LegalInformationServiceImpl(LegalInformationRepository
                                                   legalInformationRepository,
                                       EntityManager entityManager) {
        this.legalInformationRepository = legalInformationRepository;
        this.entityManager = entityManager;
    }

    @Override
    public List<LegalInformationDTO> findAll(boolean isDeleted) {
        /*
        * TODO: try-with-resources
        *  */
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedLegalInformation");
        filter.setParameter("isDeleted", isDeleted);
        List<LegalInformationDTO> legalInformationList = legalInformationRepository
                .findAll()
                .stream()
                .map(LegalInformationMapper.INSTANCE::legalInformationToDto)
                .collect(Collectors.toList());
        session.disableFilter("deletedLegalInformation");
        return legalInformationList;
    }

    @Override
    public LegalInformationDTO findById(Long id) {
        return LegalInformationMapper
                .INSTANCE
                .legalInformationToDto(legalInformationRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(LegalInformationDTO legalInformationDTO) {
        legalInformationRepository.save(LegalInformationMapper
                .INSTANCE
                .legalInformationToModel(legalInformationDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (legalInformationRepository.existsById(id)) {
            legalInformationRepository.deleteById(id);
        }
    }
}
