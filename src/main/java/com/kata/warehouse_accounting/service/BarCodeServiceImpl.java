package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.BarCodeDTO;
import com.kata.warehouse_accounting.model.mapper.BarCodeMapper;
import com.kata.warehouse_accounting.repository.BarCodeRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class BarCodeServiceImpl implements BarCodeService {
    private final BarCodeRepository barCodeRepository;

    public BarCodeServiceImpl(BarCodeRepository barCodeRepository) {
        this.barCodeRepository = barCodeRepository;
    }

    @Override
    public List<BarCodeDTO> findAll() {
        List<BarCodeDTO> barCodeDTOList = barCodeRepository.findAll().stream()
                .map(BarCodeMapper.INSTANCE::toDto)
                .collect(Collectors.toList());
        return barCodeDTOList;
    }

    @Override
    public BarCodeDTO findById(Long id) {
        return BarCodeMapper.INSTANCE.toDto(barCodeRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(BarCodeDTO barCodeDTO) {
        barCodeRepository.save(BarCodeMapper.INSTANCE.toModel(barCodeDTO));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (barCodeRepository.existsById(id)) {
            barCodeRepository.deleteById(id);
        }
    }
}
