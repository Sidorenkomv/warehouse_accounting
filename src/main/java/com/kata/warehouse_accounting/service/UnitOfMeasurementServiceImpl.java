package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.UnitMeasurementDTO;
import com.kata.warehouse_accounting.model.mapper.UnitMeasurementMapper;
import com.kata.warehouse_accounting.repository.UnitOfMeasurementRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class UnitOfMeasurementServiceImpl implements UnitOfMeasurementService {
    private final UnitOfMeasurementRepository unitOfMeasurementRepository;

    public UnitOfMeasurementServiceImpl(UnitOfMeasurementRepository unitOfMeasurementRepository) {
        this.unitOfMeasurementRepository = unitOfMeasurementRepository;
    }

    @Override
    public List<UnitMeasurementDTO> getAll() {
        return unitOfMeasurementRepository.findAll()
                .stream()
                .map(UnitMeasurementMapper.INSTANCE::unitOfMeasurementToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public UnitMeasurementDTO getById(Long id) {
        return UnitMeasurementMapper
                .INSTANCE
                .unitOfMeasurementToDTO(unitOfMeasurementRepository.findById(id).orElse(null));
    }

    @Override
    public void create(UnitMeasurementDTO unitMeasurementDTO) {
        unitOfMeasurementRepository.save(UnitMeasurementMapper
                .INSTANCE
                .unitOfMeasurementDTOToModel(unitMeasurementDTO));
    }

    @Override
    public void update(UnitMeasurementDTO unitMeasurementDTO) {
        unitOfMeasurementRepository.save(UnitMeasurementMapper
                .INSTANCE
                .unitOfMeasurementDTOToModel(unitMeasurementDTO));
    }

    @Override
    public void deleteById(Long id) {
        unitOfMeasurementRepository.deleteById(id);
    }

}
