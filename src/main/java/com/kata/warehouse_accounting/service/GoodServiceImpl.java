package com.kata.warehouse_accounting.service;


import com.kata.warehouse_accounting.model.Good;
import com.kata.warehouse_accounting.model.dto.GoodDTO;
import com.kata.warehouse_accounting.model.mapper.GoodMapper;
import com.kata.warehouse_accounting.repository.GoodRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class GoodServiceImpl implements GoodService {

    GoodRepository goodRepository;


    @Autowired
    public GoodServiceImpl(GoodRepository goodRepository) {
        this.goodRepository = goodRepository;
    }

    @Override
    public List<GoodDTO> showAll() {
        List<GoodDTO> goodsDTO = new ArrayList<>();
        List<Good> goods = goodRepository.findAll();
        for (Good good : goods) {
            goodsDTO.add(GoodMapper.GOOD_MAPPER.GoodToDTO(good));
        }
        return goodsDTO;
    }

    @Override
    public GoodDTO showById(Long id) {
        return GoodMapper
                .GOOD_MAPPER
                .GoodToDTO(goodRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void addGood(GoodDTO goodDTO) {
        goodRepository.save(GoodMapper.GOOD_MAPPER.GoodToModel(goodDTO));
    }

    @Override
    @Transactional
    public void deleteGood(Long id) {
        if (goodRepository.existsById(id)) {
            goodRepository.deleteById(id);
        }

    }

    @Override
    @Transactional
    public void editGood(GoodDTO goodDTO) {
        goodRepository.save(GoodMapper.GOOD_MAPPER.GoodToModel(goodDTO));
    }
}
