package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.CounterpartyRequestDTO;
import com.kata.warehouse_accounting.model.dto.CounterpartyResponseDTO;

import java.util.List;

public interface CounterpartyService {
    List<CounterpartyResponseDTO> findAll();

    CounterpartyResponseDTO findById(Long id);

    void save(CounterpartyRequestDTO counterpartyRequestDTO);

    void deleteById(Long id);
}
