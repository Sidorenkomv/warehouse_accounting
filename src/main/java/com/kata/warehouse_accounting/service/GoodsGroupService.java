package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.GoodsGroupDTO;

import java.util.List;

public interface GoodsGroupService {

    List<GoodsGroupDTO> showAllGroups();

    GoodsGroupDTO showGroupById(Long id);

    void addGroup(GoodsGroupDTO goodsGroup);

    void deleteGroup(Long id);

    void editGroup(GoodsGroupDTO goodsGroup);


}
