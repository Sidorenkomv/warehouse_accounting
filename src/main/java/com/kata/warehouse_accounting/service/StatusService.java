package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.StatusDTO;

import java.util.List;

public interface StatusService {
    List<StatusDTO> findAll();

    void save(StatusDTO statusDTO);

    void deleteById(Long id);
}
