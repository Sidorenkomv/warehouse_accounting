package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ProductVarietyDTO;

import java.util.List;

public interface ProductVarietyService {

    List<ProductVarietyDTO> findAll();

    ProductVarietyDTO findById(Long id);

    void save(ProductVarietyDTO productVarietyDTO);

    void deleteById(Long id);
}
