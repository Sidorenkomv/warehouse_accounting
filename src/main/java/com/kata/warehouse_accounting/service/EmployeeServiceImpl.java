package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.EmployeeDTO;
import com.kata.warehouse_accounting.model.mapper.EmployeeMapper;
import com.kata.warehouse_accounting.repository.EmployeeRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService{
    private final EmployeeRepository employeeRepository;

    public EmployeeServiceImpl(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    @Override
    public List<EmployeeDTO> getAll() {
        return employeeRepository.findAll()
                .stream()
                .map(EmployeeMapper.INSTANCE::employeeToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public EmployeeDTO getById(Long id) {
        return EmployeeMapper
                .INSTANCE
                .employeeToDTO(employeeRepository.findById(id).orElse(null));
    }

    @Override
    public void create(EmployeeDTO employeeDTO) {
        employeeRepository.save(EmployeeMapper
                .INSTANCE
                .employeeDTOToModel(employeeDTO));
    }

    @Override
    public void update(EmployeeDTO employeeDTO) {
        employeeRepository.save(EmployeeMapper
                .INSTANCE
                .employeeDTOToModel(employeeDTO));
    }

    @Override
    public void deleteById(Long id) {
        employeeRepository.deleteById(id);
    }

}
