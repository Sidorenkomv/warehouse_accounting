package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.GoodDTO;

import java.util.List;

public interface GoodService {

    List<GoodDTO> showAll();

    GoodDTO showById(Long id);

    void addGood(GoodDTO goodDTO);

    void deleteGood(Long id);

    void editGood(GoodDTO goodDTO);
}
