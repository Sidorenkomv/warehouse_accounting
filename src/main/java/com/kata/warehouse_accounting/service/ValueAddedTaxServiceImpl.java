package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ValueAddedTaxDTO;
import com.kata.warehouse_accounting.model.mapper.ValueAddedTaxMapper;
import com.kata.warehouse_accounting.repository.ValueAddedTaxRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ValueAddedTaxServiceImpl implements ValueAddedTaxService {
    private final ValueAddedTaxRepository vatRepository;

    public ValueAddedTaxServiceImpl(ValueAddedTaxRepository vatRepository) {
        this.vatRepository = vatRepository;
    }

    @Override
    public List<ValueAddedTaxDTO> findAll() {
        List<ValueAddedTaxDTO> taxDTOList = vatRepository.findAll().stream()
                .map(ValueAddedTaxMapper.INSTANCE::toDto)
                .collect(Collectors.toList());
        return taxDTOList;
    }

    @Override
    public ValueAddedTaxDTO findById(Long id) {
        return ValueAddedTaxMapper.INSTANCE.toDto(vatRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(ValueAddedTaxDTO vatDto) {
        vatRepository.save(ValueAddedTaxMapper.INSTANCE.toModel(vatDto));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (vatRepository.existsById(id)) {
            vatRepository.deleteById(id);
        }
    }
}
