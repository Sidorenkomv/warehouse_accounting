package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.LegalDetailsDTO;
import com.kata.warehouse_accounting.model.mapper.LegalDetailsMapper;
import com.kata.warehouse_accounting.repository.LegalDetailsRepository;
import org.hibernate.Filter;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LegalDetailsServiceImpl implements LegalDetailsService {

    private final LegalDetailsRepository legalDetailsRepository;
    private final EntityManager  entityManager;

    @Autowired
    public LegalDetailsServiceImpl(LegalDetailsRepository
                                               legalDetailsRepository,
                                   EntityManager entityManager) {
        this.legalDetailsRepository = legalDetailsRepository;
        this.entityManager = entityManager;
    }

    @Override
    public List<LegalDetailsDTO> findAll(boolean isDeleted) {
        /*
         * TODO: try-with-resources
         * */
        Session session = entityManager.unwrap(Session.class);
        Filter filter = session.enableFilter("deletedLegalDetails");
        filter.setParameter("isDeleted", isDeleted);
        List<LegalDetailsDTO> legalDetailsList = legalDetailsRepository
                .findAll()
                .stream()
                .map(LegalDetailsMapper.INSTANCE::legalDetailsToDto)
                .collect(Collectors.toList());
        session.disableFilter("deletedLegalDetails");
        return legalDetailsList;
    }

    @Override
    public LegalDetailsDTO findById(Long id) {
        return LegalDetailsMapper
                .INSTANCE
                .legalDetailsToDto(legalDetailsRepository.findById(id).orElse(null));
    }

    @Override
    @Transactional
    public void save(LegalDetailsDTO legalDetailDto) {
        legalDetailsRepository.save(LegalDetailsMapper
                .INSTANCE
                .legalDetailsToModel(legalDetailDto));
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        if (legalDetailsRepository.existsById(id)) {
            legalDetailsRepository.deleteById(id);
        }
    }
}
