package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.UnitMeasurementDTO;

import java.util.List;

public interface UnitOfMeasurementService {
    List<UnitMeasurementDTO> getAll();

    UnitMeasurementDTO getById(Long id);

    void create(UnitMeasurementDTO unitMeasurementDTO);

    void update(UnitMeasurementDTO unitMeasurementDTO);

    void deleteById(Long id);

}
