package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.IndicationDTO;
import java.util.List;

public interface IndicationService {
    List<IndicationDTO> getAll();

    IndicationDTO getById(Long id);

    void create(IndicationDTO indicationDTO);

    void update(IndicationDTO indicationDTO);

    void deleteById(Long id);
}
