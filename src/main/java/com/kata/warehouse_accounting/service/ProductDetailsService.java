package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ProductDetailsDTO;

import java.util.List;

public interface ProductDetailsService {

    List<ProductDetailsDTO> findAll();

    ProductDetailsDTO findById(Long id);

    void save(ProductDetailsDTO productDetailsDTO);

    void deleteById(Long id);
}
