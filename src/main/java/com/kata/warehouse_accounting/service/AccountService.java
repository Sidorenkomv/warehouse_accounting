package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.AccountDTO;

import java.util.List;

public interface AccountService {
    List<AccountDTO> getAll();

    AccountDTO getById(Long id);

    void create(AccountDTO bankAccountDTO);

    void update(AccountDTO bankAccountDTO);

    void deleteById(Long id);

}
