package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.CurrencyDTO;
import com.kata.warehouse_accounting.model.mapper.CurrencyMapper;
import com.kata.warehouse_accounting.repository.CurrencyRepository;
import com.kata.warehouse_accounting.service.CurrencyService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class CurrencyServiceImpl implements CurrencyService {
    private final CurrencyRepository currencyRepository;

    public CurrencyServiceImpl(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    @Override
    public List<CurrencyDTO> getAll() {
        return currencyRepository.findAll()
                .stream()
                .map(CurrencyMapper.INSTANCE::currencyToDTO)
                .collect(Collectors.toList());
    }

    @Override
    public CurrencyDTO getById(Long id) {
        return CurrencyMapper
                .INSTANCE
                .currencyToDTO(currencyRepository.findById(id).orElse(null));
    }

    @Override
    public void create(CurrencyDTO currencyDTO) {
        currencyRepository.save(CurrencyMapper
                .INSTANCE
                .currencyDTOToModel(currencyDTO));
    }

    @Override
    public void update(CurrencyDTO currencyDTO) {
        currencyRepository.save(CurrencyMapper
                .INSTANCE
                .currencyDTOToModel(currencyDTO));
    }

    @Override
    public void deleteById(Long id) {
        currencyRepository.deleteById(id);
    }

}
