package com.kata.warehouse_accounting.service;

import com.kata.warehouse_accounting.model.dto.ValueAddedTaxDTO;
import java.util.List;

public interface ValueAddedTaxService {
    List<ValueAddedTaxDTO> findAll();

    ValueAddedTaxDTO findById(Long id);

    void save(ValueAddedTaxDTO valueAddedTaxDTO);

    void deleteById(Long id);


}
