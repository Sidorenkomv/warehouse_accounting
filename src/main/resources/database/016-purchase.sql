INSERT INTO public.purchases (received, enabled, balance, reserve, expectation, weight, volume, sumTax, deleted)
VALUES (200.56, 1234.72, 10000, 2000, 20000, 550, 1000, 52000, false);
INSERT INTO public.purchases (received, enabled, balance, reserve, expectation, weight, volume, sumTax, deleted)
VALUES (356.0, 1600.72, 5000, 1000, 30000, 760, 1235, 76600, false);
INSERT INTO public.purchases (received, enabled, balance, reserve, expectation, weight, volume, sumTax, deleted)
VALUES (1006, 20.5, 50, 200, 10000, 200, 100, 67540, false);
INSERT INTO public.purchases (received, enabled, balance, reserve, expectation, weight, volume, sumTax, deleted)
VALUES (10.4, 95.34, 150, 50, 300, 120, 120, 5630, false);
