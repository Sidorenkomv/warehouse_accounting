INSERT INTO payment_account (address, bank, bic, cor_account, deleted, pay_account)
VALUES ('г Владимир', 'УФК по Владимирской области', '011708377', '
30101810545250000930', false, '30301810000006000001');

INSERT INTO payment_account (address, bank, bic, cor_account, deleted, pay_account)
VALUES ('г Москва', 'КУ АКБ "МОСТРАНСБАНК" ОАО - ГК "АСВ"', '044525568', '
30101810645250000568', false, '30101810600050000568');

INSERT INTO payment_account (address, bank, bic, cor_account, deleted, pay_account)
VALUES ('г Москва', 'КУ КБ "Русский ипотечный банк"', '044525526', '
30101810645250000526', false, '30101810645250000000');

INSERT INTO payment_account (address, bank, bic, cor_account, deleted, pay_account)
VALUES ('г Москва', 'Ликвидатор ООО КБ "ПЛАТИНА" - ГК "АСВ"', '044525931', '
30101810845250000931', false, '30101810845250000001');

INSERT INTO payment_account (address, bank, bic, cor_account, deleted, pay_account)
VALUES ('г Москва', 'ООО "ПроКоммерцБанк"', '044525699', '
30101810145250000699', false, '30100000145250000009');