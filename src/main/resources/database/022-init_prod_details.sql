-- Одежда
insert into product_details (age_category, c_n_f_e_a, deleted, is_partial_sale, is_set, is_traceable, model, target_gender, type_of_production,product_variety_id)
values (null,'4203100001',false,false,false,false,null,'Мужская','Произведено в РФ',1);
-- Постельное белье
insert into product_details (age_category, c_n_f_e_a, deleted, is_partial_sale, is_set, is_traceable, model, target_gender, type_of_production,product_variety_id)
values ('Взрослое','6302299000',false,false,true,false,null,null,'Произведено в РФ',2);
-- Духи и туалетная вода
insert into product_details (age_category, c_n_f_e_a, deleted, is_partial_sale, is_set, is_traceable, model, target_gender, type_of_production,product_variety_id)
values (null,'3303001000',false,false,false,false,null,null,'Произведено в РФ',3);
-- Обувь
insert into product_details (age_category, c_n_f_e_a, deleted, is_partial_sale, is_set, is_traceable, model, target_gender, type_of_production,product_variety_id)
values ('Женская','6402999300',false,false,false,false,null,null,'Ввезен в РФ',4);
-- Молочная продукция
insert into product_details (age_category, c_n_f_e_a, deleted, is_partial_sale, is_set, is_traceable, model, target_gender, type_of_production,product_variety_id)
values (null,'0401101000',false,false,false,false,null,null,'Произведено в РФ',5);