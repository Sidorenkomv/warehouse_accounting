INSERT INTO counterparty (cod,
                          comment,
                          comment_address,
                          date_of_change,
                          deleted,
                          discount_number,
                          email,
                          fax,
                          owner_department,
                          external_code,
                          phone,
                          prices,
                          address_id,
                          legal_details_id,
                          owner_employee_id,
                          status_id,
                          who_changed_it_id)
VALUES ('1528jhkh',
        'комментарий отсутствует',
        'комментарий отсутствует',
        '2022-12-07T07:22:49.666Z',
        false,
        '78789789',
        '001_user@yandex.ru',
        89990009900,
        'основной',
        'qffEI2PZjvJsvTx6Ybfef2',
        80009990099,
        1,
        2,
        2,
        1,
        2,
        1);

INSERT INTO counterparty_groups (counterparty_id, groups_counterparty_id)
VALUES (1, 1);

INSERT INTO counterparty_contact_persons (counterparty_id, contact_persons_id)
VALUES (1, 1);

INSERT INTO counterparty_payment_account (counterparty_id, payment_account_id)
VALUES (1, 1);

INSERT INTO counterparty (cod,
                          comment,
                          comment_address,
                          date_of_change,
                          deleted,
                          discount_number,
                          email,
                          fax,
                          owner_department,
                          external_code,
                          phone,
                          prices,
                          address_id,
                          legal_details_id,
                          owner_employee_id,
                          status_id,
                          who_changed_it_id)
VALUES ('8787rthy',
        'комментарий отсутствует',
        'комментарий отсутствует',
        '2022-12-08T07:22:49.666Z',
        false,
        '79887978',
        '002_user@yandex.ru',
        87770009900,
        'основной',
        'qffE-5fjejvJsvTx6Ybfef2',
        80009990000,
        2,
        2,
        2,
        4,
        1,
        2);

INSERT INTO counterparty_groups (counterparty_id, groups_counterparty_id)
VALUES (2, 2);

INSERT INTO counterparty_contact_persons (counterparty_id, contact_persons_id)
VALUES (2, 2);

INSERT INTO counterparty_payment_account (counterparty_id, payment_account_id)
VALUES (2, 2);

INSERT INTO counterparty (cod,
                          comment,
                          comment_address,
                          date_of_change,
                          deleted,
                          discount_number,
                          email,
                          fax,
                          owner_department,
                          external_code,
                          phone,
                          prices,
                          address_id,
                          legal_details_id,
                          owner_employee_id,
                          status_id,
                          who_changed_it_id)
VALUES ('fghfg5857',
        'комментарий отсутствует',
        'комментарий отсутствует',
        '2022-12-09T07:22:49.666Z',
        false,
        '3248943',
        '003_user@yandex.ru',
        88880009900,
        'основной',
        'qffEI2PZjvJsvTx6Ybfef2',
        80007770099,
        4,
        1,
        1,
        1,
        3,
        3);

INSERT INTO counterparty_groups (counterparty_id, groups_counterparty_id)
VALUES (3, 3);

INSERT INTO counterparty_contact_persons (counterparty_id, contact_persons_id)
VALUES (3, 3);

INSERT INTO counterparty_payment_account (counterparty_id, payment_account_id)
VALUES (3, 3);



INSERT INTO counterparty (cod,
                          comment,
                          comment_address,
                          date_of_change,
                          deleted,
                          discount_number,
                          email,
                          fax,
                          owner_department,
                          external_code,
                          phone,
                          prices,
                          address_id,
                          legal_details_id,
                          owner_employee_id,
                          status_id,
                          who_changed_it_id)
VALUES ('4984edgd',
        'комментарий отсутствует',
        'комментарий отсутствует',
        '2022-12-10T07:22:49.666Z',
        false,
        '1565481',
        '004_user@yandex.ru',
        89029052724,
        'основной',
        'dfgdfgd4f65d4ge6dgeeg',
        89009009690,
        2,
        1,
        2,
        5,
        1,
        1);

INSERT INTO counterparty_groups (counterparty_id, groups_counterparty_id)
VALUES (4, 4);

INSERT INTO counterparty_contact_persons (counterparty_id, contact_persons_id)
VALUES (4, 4);

INSERT INTO counterparty_payment_account (counterparty_id, payment_account_id)
VALUES (4, 4);

INSERT INTO counterparty (cod,
                          comment,
                          comment_address,
                          date_of_change,
                          deleted,
                          discount_number,
                          email,
                          fax,
                          owner_department,
                          external_code,
                          phone,
                          prices,
                          address_id,
                          legal_details_id,
                          owner_employee_id,
                          status_id,
                          who_changed_it_id)
VALUES ('dfg4d654e',
        'комментарий отсутствует',
        'комментарий отсутствует',
        '2022-12-11T07:22:49.666Z',
        false,
        '23416546',
        '005_user@yandex.ru',
        89656549856,
        'основной',
        'qffEI2gegvJsvTx6Ybfef2',
        89009004455,
        1,
        2,
        2,
        1,
        2,
        1);

INSERT INTO counterparty_groups (counterparty_id, groups_counterparty_id)
VALUES (5, 5);

INSERT INTO counterparty_contact_persons (counterparty_id, contact_persons_id)
VALUES (5, 5);

INSERT INTO counterparty_payment_account (counterparty_id, payment_account_id)
VALUES (5, 5);