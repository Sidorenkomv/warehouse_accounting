insert into public.value_added_tax (comment, date_of_change, deleted, owner_department, type, vat_rate, employee_id, who_changed_it)
VALUES ('Первый комментарий','2022-12-07 10:25:00',false,'Основной','Системный',0,1,1);
insert into public.value_added_tax (comment, date_of_change, deleted, owner_department, type, vat_rate, employee_id, who_changed_it)
VALUES ('Второй комментарий','2022-12-05 11:29:00',false,'Основной','Пользовательский',10,2,3);
insert into public.value_added_tax (comment, date_of_change, deleted, owner_department, type, vat_rate, employee_id, who_changed_it)
VALUES ('Третий комментарий','2022-12-01 17:14:00',false,'Основной','Пользовательский',20,1,2);
insert into public.value_added_tax (comment, date_of_change, deleted, owner_department, type, vat_rate, employee_id, who_changed_it)
VALUES ('Четвертый комментарий','2022-12-02 17:13:00',false,'Основной','Системный',18,3,4);
insert into public.value_added_tax (comment, date_of_change, deleted, owner_department, type, vat_rate, employee_id, who_changed_it)
VALUES ('Пятый комментарий','2022-12-10 17:17:00',false,'Основной','Системный',21,1,3);