INSERT INTO contact_person (id, comment, deleted, designation, email, full_name, phone)
VALUES (1, 'комментарий отсутствует', false, 'manager', 'user001@mail.ru', 'Иванов Иван Иванович', 89509003090);

INSERT INTO contact_person (id, comment, deleted, designation, email, full_name, phone)
VALUES (2, 'комментарий отсутствует', false, 'manager', 'user002@mail.ru', 'Петров Алексей Сергеевич', 89009005050);

INSERT INTO contact_person (id, comment, deleted, designation, email, full_name, phone)
VALUES (3, 'комментарий отсутствует', false, 'manager', 'user003@mail.ru', 'Сидоров Александр Петрович', 89009001010);

INSERT INTO contact_person (id, comment, deleted, designation, email, full_name, phone)
VALUES (4, 'комментарий отсутствует', false, 'manager', 'user004@mail.ru', 'Петров Алексей Сергеевич', 89009000000);

INSERT INTO contact_person (id, comment, deleted, designation, email, full_name, phone)
VALUES (5, 'комментарий отсутствует', false, 'manager', 'user005@mail.ru', 'Семенов Артем Иванович', 89009009999);