INSERT INTO tax_system(id,
                       deleted,
                       name)
VALUES (1,
        false,
        'ОСН');

INSERT INTO tax_system(id,
                       deleted,
                       name)
VALUES (2,
        false,
        'УСН. Доходы');

INSERT INTO tax_system(id,
                       deleted,
                       name)
VALUES (3,
        false,
        'УСН. Доходы - Расходы');

INSERT INTO tax_system(id,
                       deleted,
                       name)
VALUES (4,
        false,
        'ЕСХН');

INSERT INTO tax_system(id,
                       deleted,
                       name)
VALUES (5,
        false,
        'ЕНВД');

INSERT INTO tax_system(id,
                       deleted,
                       name)
VALUES (6,
        false,
        'Патентная Система');
