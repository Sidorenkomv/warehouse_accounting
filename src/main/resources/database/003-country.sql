INSERT INTO country (id,
                     deleted,
                     short_name,
                     long_name,
                     digital_code,
                     literal_code)
VALUES (1,
        false,
        'Россия',
        'Российская Федерация',
        643,
        'RUS');

INSERT INTO country (id,
                     deleted,
                     short_name,
                     long_name,
                     digital_code,
                     literal_code)
VALUES (2,
        false,
        'Беларусь',
        'Республика Беларусь',
        112,
        'BLR');
