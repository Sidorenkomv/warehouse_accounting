INSERT INTO public.unit_of_measurement (type, short_name, full_name, digital_Code, shared_Access)
VALUES (false, 'кг', 'Килограмм', 166, true);
INSERT INTO public.unit_of_measurement (type, short_name, full_name, digital_Code, shared_Access)
VALUES (false, 'мес', 'Месяц', 362, true);
INSERT INTO public.unit_of_measurement (type, short_name, full_name, digital_Code, shared_Access)
VALUES (false, 'ч', 'Час', 356, true);
INSERT INTO public.unit_of_measurement (type, short_name, full_name, digital_Code, shared_Access)
VALUES (false, 'мм', 'Миллиметр', 003, true);