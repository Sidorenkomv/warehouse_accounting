INSERT INTO type_of_contractor (id,
                                deleted,
                                name)
VALUES (1,
        false,
        'Юридическое лицо');

INSERT INTO type_of_contractor (id,
                                deleted,
                                name)
VALUES (2,
        false,
        'Физическое лицо');

INSERT INTO type_of_contractor (id,
                                deleted,
                                name)
VALUES (3,
        false,
        'Индивидуальный предприниматель');
