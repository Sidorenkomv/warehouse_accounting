INSERT INTO public.product_group (name, description, code, external_code, deleted, product_group_id)
VALUES ('Хлебобулочные изделия', 'В данную группу входят Хлебобулочные изделия', 25425500052587, 'SM25052587L', false, 1);
INSERT INTO public.product_group (name, description, code, external_code, deleted, product_group_id)
VALUES ('Хлеб', 'В данную группу входят различные виды хлеба', 578700052587, 'SM5787052587L', false, 2);
INSERT INTO public.product_group (name, description, code, external_code, deleted, product_group_id)
VALUES ('Хлеб белый', 'В данную группу входят различные сорта белого хлеба', 578700052588, 'SM5787052588L', false, 3);
INSERT INTO public.product_group (name, description, code, external_code, deleted, product_group_id)
VALUES ('Хлеб черный', 'В данную группу входят различные сорта черного хлеба', 578700052589, 'SM5787052589L', false, 4);
