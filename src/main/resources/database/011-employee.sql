insert into employee (description, email, entrance, first_name, inn, last_name, login, owner_department, owner_employee,
                      patronymic, picture, post, role, shared_access, telephone, when_changed, who_changed)
VALUES ('без описания', 'admin@gmail.com', true, 'Иван', '420420420', 'Иванов', 'user', 'основной', null, 'Иванович',
        null, 'Администратор', 'admin', false, 89909909090, null, null);

insert into employee (description, email, entrance, first_name, inn, last_name, login, owner_department, owner_employee,
                      patronymic, picture, post, role, shared_access, telephone, when_changed, who_changed)
VALUES ('без описания', 'user@gmail.com', false, 'Василий', '150150150', 'Петров', 'user', 'основной', null,
        'Александрович', null, 'Пользователь', 'user', false, 89009009090, null, null);

insert into employee (description, email, entrance, first_name, inn, last_name, login, owner_department, owner_employee,
                      patronymic, picture, post, role, shared_access, telephone, when_changed, who_changed)
VALUES ('без описания', 'user_001@gmail.com', false, 'Артем', '200200200', 'Смышляев', 'user', 'основной', null,
        'Алексеевич', null, 'Пользователь', 'user', false, 89509509595, null, null);

insert into employee (description, email, entrance, first_name, inn, last_name, login, owner_department, owner_employee,
                      patronymic, picture, post, role, shared_access, telephone, when_changed, who_changed)
VALUES ('без описания', 'user_002@gmail.com', false, 'Петр', '600600600', 'Сидоров', 'user', 'основной', null,
        'Семенович', null, 'Пользователь', 'user', false, 89049049494, null, null);

insert into employee (description, email, entrance, first_name, inn, last_name, login, owner_department, owner_employee,
                      patronymic, picture, post, role, shared_access, telephone, when_changed, who_changed)
VALUES ('без описания', 'user_002@gmail.com', false, 'Семен', '700700700', 'Тулеев', 'user', 'основной', null,
        'Сергеевич', null, 'Пользователь', 'user', false, 88008008080, null, null);